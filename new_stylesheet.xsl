<?xml version="1.0" encoding="ISO-8859-1" ?>
	<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <!-- Creates the body of the finding aid.-->
	<xsl:template match="/ead">
		<html>
			<head>
				<link rel="stylesheet" rev="stylesheet" href="http://www.eastmanhouse.org/lib/css/eastman.traditional.css.pcss" type="text/css" media="all" />
                <link rel="stylesheet" rev="stylesheet" href="http://www.eastmanhouse.org/lib/css/eastman.traditional.print.css" type="text/css" media="print" />
		
                       
                <!-- collapse/expand code -->
				<script src="collapse-expand.js" type="text/javascript"></script>

                <!-- TOC fixed table code -->
                <script src="jquery-1.7.1.min.js" type="text/javascript"></script>
				<script>
                    $().ready(function() {
                        var $scrollingDiv = $("#scrollingDiv");
                 
                        $(window).scroll(function(){			
                            $scrollingDiv
                                .stop()
                                .animate({"marginTop": ($(window).scrollTop())}, "slow" );			
                        });
                    });
                </script>
                <script>
					<!-- Hide from old browsers
				
				/******************************************
				* Find In Page Script, Submitted and revised by Alan Koontz (alankoontz at REMOVETHISyahoo dot com)
				* Visit Dynamic Drive (www.dynamicdrive.com) for full source code
				* This notice must stay intact for use
				******************************************/
				
				//  revised by Alan Koontz May 2003
				
				var TRange = null;
				var dupeRange = null;
				var TestRange = null;
				var win = null;
				
				
				var nom = navigator.appName.toLowerCase();
				var agt = navigator.userAgent.toLowerCase();
				var is_major   = parseInt(navigator.appVersion);
				var is_minor   = parseFloat(navigator.appVersion);
				var is_ie      = (agt.indexOf("msie") != -1);
				var is_ie4up   = (is_ie && (is_major >= 4));
				var is_not_moz = (agt.indexOf('netscape')!=-1)
				var is_nav     = (nom.indexOf('netscape')!=-1);
				var is_nav4    = (is_nav && (is_major == 4));
				var is_mac     = (agt.indexOf("mac")!=-1);
				var is_gecko   = (agt.indexOf('gecko') != -1);
				var is_opera   = (agt.indexOf("opera") != -1);
				
				
				//  GECKO REVISION
				
				var is_rev=0
				if (is_gecko) {
				temp = agt.split("rv:")
				is_rev = parseFloat(temp[1])
				}
				
				
				//  USE THE FOLLOWING VARIABLE TO CONFIGURE FRAMES TO SEARCH
				//  (SELF OR CHILD FRAME)
				
				//  If you want to search another frame, change from "self" to
				//  the name of the target frame:
				//  e.g., var frametosearch = 'main'
				
				//var frametosearch = 'main';
				var frametosearch = self;
				
				
				function search(whichform, whichframe) {
				
				//  TEST FOR IE5 FOR MAC (NO DOCUMENTATION)
				
				if (is_ie4up && is_mac) return;
				
				//  TEST FOR NAV 6 (NO DOCUMENTATION)
				
				if (is_gecko && (is_rev <1)) return;
				
				//  TEST FOR Opera (NO DOCUMENTATION)
				
				if (is_opera) return;
				
				//  INITIALIZATIONS FOR FIND-IN-PAGE SEARCHES
				
				if(whichform.findthis.value!=null && whichform.findthis.value!='') {
				
					   str = whichform.findthis.value;
					   win = whichframe;
					   var frameval=false;
					   if(win!=self)
				{
				
					   frameval=true;  // this will enable Nav7 to search child frame
					   win = parent.frames[whichframe];
				
				}
				
					
				}
				
				else return;  //  i.e., no search string was entered
				
				var strFound;
				
				//  NAVIGATOR 4 SPECIFIC CODE
				
				if(is_nav4 && (is_minor < 5)) {
				   
				  strFound=win.find(str); // case insensitive, forward search by default
				
				//  There are 3 arguments available:
				//  searchString: type string and it's the item to be searched
				//  caseSensitive: boolean, is search case sensitive?
				//  backwards: boolean, should we also search backwards?
				//  strFound=win.find(str, false, false) is the explicit
				//  version of the above
				//  The Mac version of Nav4 has wrapAround, but
				//  cannot be specified in JS
				
				 
						}
				
				//  NAVIGATOR 7 and Mozilla rev 1+ SPECIFIC CODE (WILL NOT WORK WITH NAVIGATOR 6)
				
				if (is_gecko && (is_rev >= 1)) {
				   
					if(frameval!=false) win.focus(); // force search in specified child frame
					strFound=win.find(str, false, false, true, false, frameval, false);
				
				//  The following statement enables reversion of focus 
				//  back to the search box after each search event 
				//  allowing the user to press the ENTER key instead
				//  of clicking the search button to continue search.
				//  Note: tends to be buggy in Mozilla as of 1.3.1
				//  (see www.mozilla.org) so is excluded from users 
				//  of that browser.
				
					if (is_not_moz)  whichform.findthis.focus();
				
				//  There are 7 arguments available:
				//  searchString: type string and it's the item to be searched
				//  caseSensitive: boolean, is search case sensitive?
				//  backwards: boolean, should we also search backwards?
				//  wrapAround: boolean, should we wrap the search?
				//  wholeWord: boolean: should we search only for whole words
				//  searchInFrames: boolea, should we search in frames?
				//  showDialog: boolean, should we show the Find Dialog?
				
				
				}
				
				 if (is_ie4up) {
				
				  // EXPLORER-SPECIFIC CODE revised 5/21/03
				
				  if (TRange!=null) {
					  
				   TestRange=win.document.body.createTextRange();
				 
					  
				
				   if (dupeRange.inRange(TestRange)) {
				
				   TRange.collapse(false);
				   strFound=TRange.findText(str);
					if (strFound) {
						//the following line added by Mike and Susan Keenan, 7 June 2003
						win.document.body.scrollTop = win.document.body.scrollTop + TRange.offsetTop;
						TRange.select();
						}
				
				
				   }
				   
				   else {
				
					 TRange=win.document.body.createTextRange();
					 TRange.collapse(false);
					 strFound=TRange.findText(str);
					 if (strFound) {
						//the following line added by Mike and Susan Keenan, 7 June 2003
						win.document.body.scrollTop = TRange.offsetTop;
						TRange.select();
						}
				
				
				
				   }
				  }
				  
				   if (TRange==null || strFound==0) {
				   TRange=win.document.body.createTextRange();
				   dupeRange = TRange.duplicate();
				   strFound=TRange.findText(str);
					if (strFound) {
						//the following line added by Mike and Susan Keenan, 7 June 2003
						win.document.body.scrollTop = TRange.offsetTop;
						TRange.select();
						}
				
				   
				   }
				
				 }
				
				  if (!strFound) alert ("String '"+str+"' not found!") // string not found
				
						
				}
				// -->
				</script>
                
                            
				<title>
					<xsl:value-of select="eadheader/filedesc/titlestmt/titleproper"/>
					<xsl:text>  </xsl:text>
					<xsl:value-of select="eadheader/filedesc/titlestmt/subtitle"/>
				</title>
                
		        
        		<xsl:call-template name="metadata"/>
			</head>
	
	<!--This part of the template creates a table for the finding aid with
	two columns. -->
            <body onload="collapseAll();">
            <div id="container">
                <div id="logo"><a href="#"></a></div>
                        
                <div class="gehMenu">
                <p style="line-height:2em; text-align:center;">
                        You are viewing the finding aid for <xsl:value-of select="eadheader/filedesc/titlestmt/titleproper"/>. <a href="http://www.eastmanhouse.org/collections/photography.php">Click here</a> to return to the photography collection.
                        </p>
                </div>
                
            <div style=" height: 18px; clear: both;" > </div>
                
               <div id="content">
                    <div id="sidebar">
                        <div id="sidebar-head">
                            &#160;
                        </div>
                    	<div id="scrollingDiv">
                            <div id="sidebar-body">
                                <div id="searchContainer">
                                <h2>Search</h2>
                                    <div id="search">                  
                                    <form name="form1" onSubmit="search(document.form1, frametosearch); return false">
                                        <div>
                                        <input type="text" name="findthis" size="15" title="Press 'ALT s' after clicking submit to repeatedly search page" />
										<input type="submit" value="Search" ACCESSKEY="s" />
                                        </div>
                                    </form>
                                    </div> 
                                </div>    
                                <div id="navContainer">
                                    <xsl:call-template name="toc"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="infobar">
                        <div id="breadcrumb">
                        <p><a class="main" href="/">Home</a> &#187; <a class="education" href="/education">Education</a> &#187; <a class="education-k-12" href="/education/k-12">K-12</a></p>
                        </div>
                        <div id="tools">
                        <p><a href="#">Share</a> | <a href="#">Print</a> | <a href="#">Etc.</a></p>
                        </div>
                    </div>
                    <div id="page">
                        <xsl:apply-templates select="eadheader"/>
                        
                        <!--To change the order of display, adjust the sequence of
                        the following apply-template statements which invoke the various
                        templates that populate the finding aid.  Multiple statements
                        are included to handle the possibility that descgrp has been used
                        as a wrapper to replace add and admininfo.  In several cases where
                        multiple elemnents are displayed together in the output, a call-template
                        statement is used-->	
                        
                        <xsl:apply-templates select="archdesc/did"/>
                        <xsl:apply-templates select="archdesc/bioghist"/>
                        <xsl:apply-templates select="archdesc/scopecontent"/>
                        <xsl:apply-templates select="archdesc/arrangement"/>
                        <xsl:call-template name="archdesc-restrict"/>
                        <xsl:call-template name="archdesc-relatedmaterial"/>
                        <xsl:apply-templates select="archdesc/controlaccess"/>
                        <xsl:apply-templates select="archdesc/odd"/>
                        <xsl:apply-templates select="archdesc/originalsloc"/>
                        <xsl:apply-templates select="archdesc/phystech"/>
                        <xsl:call-template name="archdesc-admininfo"/>
                        <xsl:apply-templates select="archdesc/otherfindaid | archdesc/*/otherfindaid"/>
                        <xsl:apply-templates select="archdesc/fileplan | archdesc/*/fileplan"/>
                        <xsl:apply-templates select="archdesc/bibliography | archdesc/*/bibliography"/>
                        <xsl:apply-templates select="archdesc/index | archdesc/*/index"/>
                        <xsl:apply-templates select="archdesc/dsc"/>
                    </div>
                    <div id="footer">
                        <div id="footer-links">
                            <div>
                            <a class="tools-pressroom-index" href="/tools/pressroom/index.php"> Press Room</a><a class="tools-map" href="/tools/map.php">Site Map</a><a class="tools-links" href="/tools/links.php">Related Links</a><a class="tools-contact" href="/tools/contact.php">Contact Us</a> 
                            </div>
                        </div>
                    </div>
                </div>
                
                <div style="clear: both;"> </div>         
                                       
            </div>
                
            <div id="subfooter">
                <div class="content" style="padding: 10px;">
                    <p><strong>Location</strong>: 900 East Avenue, Rochester, NY 14607 &#183; <a href="/visit/location.php">More Information</a> &#183; RTS Routes <a href="http://www.rgrta.org/Data/RouteDocs/125_RTS%2017%20East.pdf">17</a>, <a href="http://www.rgrta.org/Data/RouteDocs/126_RTS%2018%20University.pdf" title="">18/19</a>, <a href="http://www.rgrta.org/Data/RouteDocs/129_RTS%2021%20Fairport.pdf" title="">21</a>, and <a href="http://www.rgrta.org/Data/RouteDocs/130_RTS%2022%20Penfield.pdf" title="">22</a></p>
                    <p><strong>Hours</strong>: Tue - Sat: 10am - 5pm &#183; Sun: 1pm - 5pm &#183; <a href="/visit/hours.php">More Information</a></p>
                    <p><strong>Admission</strong>: Adults $12, Seniors (65+) $10, Students (with ID) $5, Children 12 and Under Free &#183; <a href="/get-involved/membership.php">Members enjoy free admission</a></p>
                    <p><strong>Contact</strong>: Telephone (585) 271-3361 &#183; <a href="/tools/contact.php">Online</a> &#183; <a href="http://www.facebook.com/georgeeastmanhouse">Facebook</a> &#183; <a href="http://www.twitter.com/eastmanhouse">Twitter</a></p>
                    <p><strong>Copyright</strong>: &#169;2000-2012 George Eastman House &#183; <a href="/tools/tos.php">Terms of Service &#x26; Privacy Policy</a></p>
                    </div>
            </div>
            
            
			</body>
		</html>
	</xsl:template>
	
    
    
    
    
<!--This template creates HTML meta tags that are inserted into the HTML ouput
for use by web search engines indexing this file.   The content of each
resulting META tag uses Dublin Core semantics and is drawn from the text of
the finding aid.-->
	<xsl:template name="metadata">
		<meta http-equiv="Content-Type" name="dc.title"
		content="{eadheader/filedesc/titlestmt/titleproper&#x20; }{eadheader/filedesc/titlestmt/subtitle}"/>
		<meta http-equiv="Content-Type" name="dc.author" content="{archdesc/did/origination}"/>
		
		<xsl:for-each select="//controlaccess/persname | //controlaccess/corpname">
			<xsl:choose>
				<xsl:when test="@encodinganalog='600'">
				<meta http-equiv="Content-Type" name="dc.subject" content="{.}"/>
				</xsl:when>

				<xsl:when test="//@encodinganalog='610'">
					<meta http-equiv="Content-Type" name="dc.subject" content="{.}"/>
				</xsl:when>

				<xsl:when test="//@encodinganalog='611'">
					<meta http-equiv="Content-Type" name="dc.subject" content="{.}"/>
				</xsl:when>

				<xsl:when test="//@encodinganalog='700'">
					<meta http-equiv="Content-Type" name="dc.contributor" content="{.}"/>
				</xsl:when>

				<xsl:when test="//@encodinganalog='710'">
					<meta http-equiv="Content-Type" name="dc.contributor" content="{.}"/>
				</xsl:when>

				<xsl:otherwise>
					<meta http-equiv="Content-Type" name="dc.contributor" content="{.}"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:for-each select="//controlaccess/subject">
			<meta http-equiv="Content-Type" name="dc.subject" content="{.}"/>
		</xsl:for-each>
		<xsl:for-each select="//controlaccess/geogname">
			<meta http-equiv="Content-Type" name="dc.subject" content="{.}"/>
		</xsl:for-each>
		
		<meta http-equiv="Content-Type" name="dc.title" content="{archdesc/did/unittitle}"/>
		<meta http-equiv="Content-Type" name="dc.type" content="text"/>
		<meta http-equiv="Content-Type" name="dc.format" content="manuscripts"/>
		<meta http-equiv="Content-Type" name="dc.format" content="finding aids"/>
		
	</xsl:template>

<!--This template creates the Table of Contents column for the finding aid.-->
	<xsl:template name="toc">
		<h2>Table of Contents</h2>
		<xsl:if test="string(archdesc/did/head)">
					<a href="#{generate-id(archdesc/did/head)}">
						<xsl:value-of select="archdesc/did/head"/>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/bioghist/head)">
					<a href="#{generate-id(archdesc/bioghist/head)}">
						<xsl:value-of select="archdesc/bioghist/head"/>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/scopecontent/head)">
					<a href="#{generate-id(archdesc/scopecontent/head)}">
						<xsl:value-of select="archdesc/scopecontent/head"/>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/arrangement/head)">
					<a href="#{generate-id(archdesc/arrangement/head)}">
						<xsl:value-of select="archdesc/arrangement/head"/>
					</a>
                    <br />
		</xsl:if>
		
		<xsl:if test="string(archdesc/userestrict/head)
		or string(archdesc/accessrestrict/head)
		or string(archdesc/*/userestrict/head)
		or string(archdesc/*/accessrestrict/head)">
					<a href="#restrictlink">
						<xsl:text>Restrictions</xsl:text>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/controlaccess/head)">
					<a href="#{generate-id(archdesc/controlaccess/head)}">
						<xsl:value-of select="archdesc/controlaccess/head"/>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/relatedmaterial)
		or string(archdesc/separatedmaterial)
		or string(archdesc/*/relatedmaterial)
		or string(archdesc/*/separatedmaterial)">
					<a href="#relatedmatlink">
						<xsl:text>Related Material</xsl:text>
					</a>
                    <br />
		</xsl:if>
		<xsl:if test="string(archdesc/acqinfo/*)
		or string(archdesc/processinfo/*)
		or string(archdesc/prefercite/*)
		or string(archdesc/custodialhist/*)
		or string(archdesc/processinfo/*)
		or string(archdesc/appraisal/*)
		or string(archdesc/accruals/*)
		or string(archdesc/*/acqinfo/*)
		or string(archdesc/*/processinfo/*)
		or string(archdesc/*/prefercite/*)
		or string(archdesc/*/custodialhist/*)
		or string(archdesc/*/processinfo/*)
		or string(archdesc/*/appraisal/*)
		or string(archdesc/*/accruals/*)">
					<a href="#adminlink">
					<xsl:text>Administrative Information</xsl:text>
					</a>
                    <br />
		</xsl:if>
		
		<xsl:if test="string(archdesc/otherfindaid/head)
			or string(archdesc/*/otherfindaid/head)">
					<xsl:choose>
						<xsl:when test="archdesc/otherfindaid/head">
							<a href="#{generate-id(archdesc/otherfindaid/head)}">
								<xsl:value-of select="archdesc/otherfindaid/head"/>
							</a>
						</xsl:when>
						<xsl:when test="archdesc/*/otherfindaid/head">
							<a href="#{generate-id(archdesc/*/otherfindaid/head)}">
								<xsl:value-of select="archdesc/*/otherfindaid/head"/>
							</a>
						</xsl:when>
					</xsl:choose>
                    <br />
		</xsl:if>
		
		<!--The next test covers the situation where there is more than one odd element
			in the document.-->
		<xsl:if test="string(archdesc/odd/head)">
			<xsl:for-each select="archdesc/odd">
						<a href="#{generate-id(head)}">
							<xsl:value-of select="head"/>
						</a>
                    <br />
			</xsl:for-each>
		</xsl:if>
		
		<xsl:if test="string(archdesc/bibliography/head)
			or string(archdesc/*/bibliography/head)">
					<xsl:choose>
						<xsl:when test="archdesc/bibliography/head">
							<a href="#{generate-id(archdesc/bibliography/head)}">
								<xsl:value-of select="archdesc/bibliography/head"/>
							</a>
						</xsl:when>
						<xsl:when test="archdesc/*/bibliography/head">
							<a href="#{generate-id(archdesc/*/bibliography/head)}">
								<xsl:value-of select="archdesc/*/bibliography/head"/>
							</a>
						</xsl:when>
					</xsl:choose>
                    <br />
		</xsl:if>
		
		<xsl:if test="string(archdesc/index/head)
			or string(archdesc/*/index/head)">
					<xsl:choose>
						<xsl:when test="archdesc/index/head">
							<a href="#{generate-id(archdesc/index/head)}">
								<xsl:value-of select="archdesc/index/head"/>
							</a>
						</xsl:when>
						<xsl:when test="archdesc/*/index/head">
							<a href="#{generate-id(archdesc/*/index/head)}">
								<xsl:value-of select="archdesc/*/index/head"/>
							</a>
						</xsl:when>
					</xsl:choose>
                    <br />
		</xsl:if>
		
		<xsl:if test="string(archdesc/dsc/head)">
					<a href="#{generate-id(archdesc/dsc/head)}">
						<xsl:value-of select="archdesc/dsc/head"/>
					</a>
                    <br />

		</xsl:if>
        <!-- added Java components for scrolling TOC and collapsing sections -->
        <a href="#" onclick="expandAll();">[expand all sections
        <img src="expanded.png"
        alt="expand/collapse"></img>]
        </a><br/>
        <a href="#" onclick="collapseAll();">[collapse all sections
        <img src="collapsed.png"
        alt="collapse arrow"></img>]
        </a>
	</xsl:template>
	
<!-- The following general templates format the display of various RENDER attributes.-->
	<xsl:template match="emph[@render='bold']">
		<b>
			<xsl:apply-templates/>
		</b>
	</xsl:template>
	<xsl:template match="emph[@render='italic']">
		<i>
			<xsl:apply-templates/>
		</i>
	</xsl:template>
	<xsl:template match="emph[@render='underline']">
		<u>
			<xsl:apply-templates/>
		</u>
	</xsl:template>
	<xsl:template match="emph[@render='sub']">
		<sub>
			<xsl:apply-templates/>
		</sub>
	</xsl:template>
	<xsl:template match="emph[@render='super']">
		<super>
			<xsl:apply-templates/>
		</super>
	</xsl:template>
	
	<xsl:template match="emph[@render='quoted']">
		<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</xsl:template>
	
	<xsl:template match="emph[@render='doublequote']">
		<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</xsl:template>
	<xsl:template match="emph[@render='singlequote']">
		<xsl:text>'</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>'</xsl:text>
	</xsl:template>
	<xsl:template match="emph[@render='bolddoublequote']">
		<b>
			<xsl:text>"</xsl:text>
			<xsl:apply-templates/>
			<xsl:text>"</xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="emph[@render='boldsinglequote']">
		<b>
			<xsl:text>'</xsl:text>
			<xsl:apply-templates/>
			<xsl:text>'</xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="emph[@render='boldunderline']">
		<b>
			<u>
				<xsl:apply-templates/>
			</u>
		</b>
	</xsl:template>
	<xsl:template match="emph[@render='bolditalic']">
		<b>
			<i>
				<xsl:apply-templates/>
			</i>
		</b>
	</xsl:template>
	<xsl:template match="emph[@render='boldsmcaps']">
		<font style="font-variant: small-caps">
			<b>
				<xsl:apply-templates/>
			</b>
		</font>
	</xsl:template>
	<xsl:template match="emph[@render='smcaps']">
		<font style="font-variant: small-caps">
			<xsl:apply-templates/>
		</font>
	</xsl:template>
	<xsl:template match="title[@render='bold']">
		<b>
			<xsl:apply-templates/>
		</b>
	</xsl:template>
	<xsl:template match="title[@render='italic']">
		<i>
			<xsl:apply-templates/>
		</i>
	</xsl:template>
	<xsl:template match="title[@render='underline']">
		<u>
			<xsl:apply-templates/>
		</u>
	</xsl:template>
	<xsl:template match="title[@render='sub']">
		<sub>
			<xsl:apply-templates/>
		</sub>
	</xsl:template>
	<xsl:template match="title[@render='super']">
		<super>
			<xsl:apply-templates/>
		</super>
	</xsl:template>

	<xsl:template match="title[@render='quoted']">
		<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</xsl:template>

	<xsl:template match="title[@render='doublequote']">
		<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</xsl:template>
	
	<xsl:template match="title[@render='singlequote']">
		<xsl:text>'</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>'</xsl:text>
	</xsl:template>
	<xsl:template match="title[@render='bolddoublequote']">
		<b>
			<xsl:text>"</xsl:text>
			<xsl:apply-templates/>
			<xsl:text>"</xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="title[@render='boldsinglequote']">
		<b>
			<xsl:text>'</xsl:text>
			<xsl:apply-templates/>
			<xsl:text>'</xsl:text>
		</b>
	</xsl:template>

	<xsl:template match="title[@render='boldunderline']">
		<b>
			<u>
				<xsl:apply-templates/>
			</u>
		</b>
	</xsl:template>
	<xsl:template match="title[@render='bolditalic']">
		<b>
			<i>
				<xsl:apply-templates/>
			</i>
		</b>
	</xsl:template>
	<xsl:template match="title[@render='boldsmcaps']">
		<font style="font-variant: small-caps">
			<b>
				<xsl:apply-templates/>
			</b>
		</font>
	</xsl:template>
	<xsl:template match="title[@render='smcaps']">
		<font style="font-variant: small-caps">
			<xsl:apply-templates/>
		</font>
	</xsl:template>

<!-- This template converts a Ref element into an HTML anchor.-->
	<xsl:template match="ref">
		<a href="#{@target}">
			<xsl:apply-templates/>
		</a>
	</xsl:template>
    <xsl:template match="extref">
		<a href="{@target}">
			<xsl:apply-templates/>
		</a>
	</xsl:template>	

<!--This template rule formats a list element anywhere except in arrangement.-->
	<xsl:template match="list[parent::*[not(self::arrangement)]]/head">
		<div style="margin-left: 25pt">
			<b>
				<xsl:apply-templates/>
			</b>
		</div>
	</xsl:template>
		
	<xsl:template match="list[parent::*[not(self::arrangement)]]/item">
			<div style="margin-left: 40pt">
				<xsl:apply-templates/>
			</div>
	</xsl:template>
	
<!--Formats a simple table. The width of each column is defined by the colwidth attribute in a colspec element.-->
	<xsl:template match="table">
		<table width="75%" style="margin-left: 25pt">
			<tr>
				<td colspan="3">
					<h4>
						<xsl:apply-templates select="head"/>
					</h4>
				</td>
			</tr>
			<xsl:for-each select="tgroup">
				<tr>
					<xsl:for-each select="colspec">
						<td width="{@colwidth}"></td>
					</xsl:for-each>
				</tr>
				<xsl:for-each select="thead">
					<xsl:for-each select="row">
						<tr>
							<xsl:for-each select="entry">
								<td valign="top">
									<b>
										<xsl:apply-templates/>
									</b>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</xsl:for-each>

				<xsl:for-each select="tbody">
					<xsl:for-each select="row">
						<tr>
							<xsl:for-each select="entry">
								<td valign="top">
									<xsl:apply-templates/>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
		</table>
	</xsl:template>

<!--This template rule formats a chronlist element.-->
	<xsl:template match="chronlist">
		<table width="100%" style="margin-left:25pt">
			<tr>
				<td width="5%"> </td>
				<td width="15%"> </td>
				<td width="80%"> </td>
			</tr>
			<xsl:apply-templates/>
		</table>
	</xsl:template>
	
	<xsl:template match="chronlist/head">
		<tr>
			<td colspan="3">
				<h4>
					<xsl:apply-templates/>			
				</h4>
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match="chronlist/listhead">
		<tr>
			<td> </td>
			<td>
				<b>
					<xsl:apply-templates select="head01"/>
				</b>
			</td>
			<td>
				<b>
					<xsl:apply-templates select="head02"/>
				</b>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="chronitem">
		<!--Determine if there are event groups.-->
		<xsl:choose>
			<xsl:when test="eventgrp">
				<!--Put the date and first event on the first line.-->
				<tr>
					<td> </td>
					<td valign="top">
						<xsl:apply-templates select="date"/>
					</td>
					<td valign="top">
						<xsl:apply-templates select="eventgrp/event[position()=1]"/>
					</td>
				</tr>
				<!--Put each successive event on another line.-->
				<xsl:for-each select="eventgrp/event[not(position()=1)]">
					<tr>
						<td> </td>
						<td> </td>
						<td valign="top">
							<xsl:apply-templates select="."/>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:when>
			<!--Put the date and event on a single line.-->
			<xsl:otherwise>
				<tr>
					<td> </td>
					<td valign="top">
						<xsl:apply-templates select="date"/>
					</td>
					<td valign="top">
						<xsl:apply-templates select="event"/>
					</td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
<!--Suppreses all other elements of eadheader.-->
	<xsl:template match="eadheader">
	<h1 style="text-align:center">
		<a name="{generate-id(titlestmt/titleproper)}"></a>
			<xsl:value-of select="filedesc/titlestmt/titleproper"/>		
	</h1>
	<h2 style="text-align:center">
			<xsl:value-of select="filedesc/titlestmt/subtitle"/>
	</h2>
	<br />
    <div class="frame right">
        <img src="window.jpg" alt='window' />
    </div>
    
    </xsl:template>

<!--This template displays the did, inserts the head and then
each of the other did elements.  To change the order of appearance of these
elements, change the sequence of the apply-templates statements.-->
	<xsl:template match="archdesc/did">
            <h2>
                <a name="{generate-id(head)}">
                    <xsl:apply-templates select="head"/>
                </a>
            </h2>

	<!--One can change the order of appearance for the children of did
				by changing the order of the following statements.-->	
			<xsl:apply-templates select="repository"/>			
			<xsl:apply-templates select="origination"/>	
			<xsl:apply-templates select="unittitle"/>	
			<xsl:apply-templates select="unitdate"/>		
			<xsl:apply-templates select="physdesc"/>	
			<xsl:apply-templates select="abstract"/>	
			<xsl:apply-templates select="unitid"/>				
			<xsl:apply-templates select="langmaterial"/>
			<xsl:apply-templates select="materialspec"/>
			<xsl:apply-templates select="physloc"/>
            <xsl:apply-templates select="note"/>
		<hr></hr>
	</xsl:template>
	


<!--This template formats the repostory, origination, physdesc, abstract,
	unitid, physloc and materialspec elements of archdesc/did which share a common presentaiton.
	The sequence of their appearance is governed by the previous template.-->
	<xsl:template match="archdesc/did/repository
	| archdesc/did/origination
	| archdesc/did/physdesc
	| archdesc/did/unitid
	| archdesc/did/physloc
	| archdesc/did/abstract
	| archdesc/did/langmaterial
	| archdesc/did/materialspec">
		<!--The template tests to see if there is a label attribute,
		inserting the contents if there is or adding display textif there isn't.
		The content of the supplied label depends on the element.  To change the
		supplied label, simply alter the template below.-->
		<xsl:choose>
			<xsl:when test="@label">
				<p>
                    <b>
                        <xsl:value-of select="@label"/>
                    </b>
                <br />
                    <xsl:apply-templates/>
                </p>
			</xsl:when>
			<xsl:otherwise>
				<p>
                <b>
                <xsl:choose>
                    <xsl:when test="self::repository">
                        <xsl:text>Repository: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::origination">
                        <xsl:text>Creator: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::physdesc">
                        <xsl:text>Quantity: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::physloc">
                        <xsl:text>Location: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::unitid">
                        <xsl:text>Identification: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::abstract">
                        <xsl:text>Abstract:</xsl:text>
                    </xsl:when>
                    <xsl:when test="self::langmaterial">
                        <xsl:text>Language: </xsl:text>
                    </xsl:when>
                    <xsl:when test="self::materialspec">
                        <xsl:text>Technical: </xsl:text>
                    </xsl:when>
                </xsl:choose>
                </b>
                <br />
                    <xsl:apply-templates/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

<!-- The following two templates test for and processes various permutations
of unittitle and unitdate.-->
	<xsl:template match="archdesc/did/unittitle">
		<!--The template tests to see if there is a label attribute for unittitle,
inserting the contents if there is or adding one if there isn't. -->
		<xsl:choose>
			<xsl:when test="@label">
				<p>
                    <b><xsl:value-of select="@label"/></b>
                    <br />
                    <!--Inserts the text of unittitle and any children other that unitdate.-->	
                    <xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
				</p>
			</xsl:when>
		
			<xsl:otherwise>
				<p>
					<b><xsl:text>Title: </xsl:text></b>
					<br />
					<xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
                </p>
			</xsl:otherwise>
		</xsl:choose>
		<!--If unitdate is a child of unittitle, it inserts unitdate on a new line.  -->
		<xsl:if test="child::unitdate">
			<!--The template tests to see if there is a label attribute for unittitle,
			inserting the contents if there is or adding one if there isn't. -->
			<xsl:choose>
				<xsl:when test="unitdate/@label">
					<p>
						<b><xsl:value-of select="unitdate/@label"/></b>
						<br />
						<xsl:apply-templates select="unitdate"/>
                    </p>
				</xsl:when>
	
				<xsl:otherwise>
						<p>
                        <b><xsl:text>Dates: </xsl:text></b>
						<br />
						<xsl:apply-templates select="unitdate"/>
						</p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!-- Processes the unit date if it is not a child of unit title but a child of did, the current context.-->
	<xsl:template match="archdesc/did/unitdate">

		<!--The template tests to see if there is a label attribute for a unittitle that is the
	child of did and not unittitle, inserting the contents if there is or adding one if there isn't.-->
		<xsl:choose>
			<xsl:when test="@label">
					<p>
						<b><xsl:value-of select="@label"/></b>
						<br />
						<xsl:apply-templates/>
					</p>
			</xsl:when>
		
			<xsl:otherwise>
					<p>
						<b><xsl:text>Dates: </xsl:text></b>
                        <br />
						<xsl:apply-templates/>
					</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

<!--This template processes the note element.-->
	<xsl:template match="archdesc/did/note">
		<xsl:for-each select="p">
			<!--The template tests to see if there is a label attribute,inserting the contents if there is or adding one if there isn't. -->
			<xsl:choose>
				<xsl:when test="parent::note[@label]">
					<!--This nested choose tests for and processes the first paragraph. Additional paragraphs do not get a label.-->
					<xsl:choose>
						<xsl:when test="position()=1">
								<p>
									<b><xsl:value-of select="@label"/></b>
									<br />
									<xsl:apply-templates/>
								</p>
						</xsl:when>

						<xsl:otherwise>
								<p>
									<xsl:apply-templates/>
								</p>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<!--Processes situations where there is no
					label attribute by supplying default text.-->
				<xsl:otherwise>
					<!--This nested choose tests for and processes the first paragraph. Additional paragraphs do not get a label.-->
					<xsl:choose>
						<xsl:when test="position()=1">
								<p>
									<b><xsl:text>Note: </xsl:text></b>
									<br />
									<xsl:apply-templates/>
								</p>
						</xsl:when>
		
						<xsl:otherwise>
								<p>
									<xsl:apply-templates/>
								</p>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<!--Closes each paragraph-->
		</xsl:for-each>
	</xsl:template>

<!--This template rule formats the top-level bioghist element -->
	<xsl:template match="archdesc/bioghist |
			archdesc/scopecontent |
			archdesc/phystech |
			archdesc/odd   |
			archdesc/arrangement">
		<xsl:if test="string(child::*)">	
			<xsl:apply-templates/>
			<hr></hr>
		</xsl:if>
	</xsl:template>

<!--This template formats various head elements and makes them targets for
		links from the Table of Contents.-->
	<xsl:template match="archdesc/bioghist/head  |
			archdesc/scopecontent/head |
			archdesc/arrangement/head |
			archdesc/phystech/head |
			archdesc/controlaccess/head |
			archdesc/odd/head">
		<h3>
			<a name="{generate-id()}">
				<xsl:apply-templates/>
			</a>
		</h3>
	</xsl:template>

	<xsl:template match="archdesc/bioghist/p |
			archdesc/scopecontent/p |
			archdesc/arrangement/p |
			archdesc/phystech/p |
			archdesc/controlaccess/p |
			archdesc/odd/p |
			archdesc/bioghist/note/p |
			archdesc/scopecontent/note/p |
			archdesc/arrangement/note/p |
			archdesc/phystech/note/p |
			archdesc/controlaccess/note/p |
			archdesc/odd/note/p">
		<p style="margin-left:25pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	
	<xsl:template match="archdesc/bioghist/bioghist/head |
		archdesc/scopecontent/scopecontent/head">
		<h3 style="margin-left:25pt">
			<xsl:apply-templates/>
		</h3>
	</xsl:template>
	
	<xsl:template match="archdesc/bioghist/bioghist/p |
		archdesc/scopecontent/scopecontent/p |
		archdesc/bioghist/bioghist/note/p |
		archdesc/scopecontent/scopecontent/note/p">
		<p style="margin-left: 50pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>

<!-- The next two templates format an arrangement
	statement embedded in <scopecontent>.-->
	 	       
        <xsl:template match="archdesc/scopecontent/arrangement/head">
                <a name="{generate-id(head)}">
                    <xsl:apply-templates/>
                </a>
        </xsl:template>
        
        <xsl:template match="archdesc/scopecontent/arrangement/p
        | archdesc/scopecontent/arrangement/note/p">
            <p style="margin-left:50pt">
                <xsl:apply-templates/>
            </p>
        </xsl:template>

<!-- The next three templates format a list within an arrangement
	statement whether it is directly within <archdesc> or embedded in
	<scopecontent>.-->
			
	<xsl:template match="archdesc/scopecontent/arrangement/list/head">
			<a name="{generate-id(head)}">
				<xsl:apply-templates/>
			</a>
	</xsl:template>
	
	<xsl:template match="archdesc/arrangement/list/head">
			<a name="{generate-id()}">
				<xsl:apply-templates/>
			</a>
	</xsl:template>
	
	<xsl:template match="archdesc/scopecontent/arrangement/list/item
	| archdesc/arrangement/list/item">
			<a>
				<xsl:attribute name="href">#series<xsl:number/>
                </xsl:attribute>
				<xsl:apply-templates/>
			</a>&#160;&#160;
	</xsl:template>

<!--This template rule formats the top-level related material
	elements by combining any related or separated materials
	elements. It begins by testing to see if there related or separated
	materials elements with content.-->
	<xsl:template name="archdesc-relatedmaterial">
		<xsl:if test="string(archdesc/relatedmaterial) or
		string(archdesc/*/relatedmaterial) or
		string(archdesc/separatedmaterial) or
		string(archdesc/*/separatedmaterial) or 
        string(archdesc/*/accruals/*)">
			<h3>
				<xsl:element name="a">
                <xsl:attribute name="href">
                <xsl:text>javascript:changeDisplay('relatedmaterial');</xsl:text>
                </xsl:attribute>
                <img id="xrelatedmaterial"
                src="expanded.png"
                alt="expand/collapse"/>
                </xsl:element>
				<a name="relatedmatlink">
					<b>
						<xsl:text>Related Material</xsl:text>
					</b>
				</a>
			</h3>
            <div id="relatedmaterial" class="showhide" style="display: block;">
			<xsl:apply-templates select="archdesc/relatedmaterial/p
				| archdesc/*/relatedmaterial/p
				| archdesc/relatedmaterial/note/p
				| archdesc/*/relatedmaterial/note/p"/>
			<xsl:apply-templates select="archdesc/separatedmaterial/p
				| archdesc/*/separatedmaterial/p
				| archdesc/separatedmaterial/note/p
				| archdesc/*/separatedmaterial/note/p"/>
            </div>
			<hr></hr>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="archdesc/relatedmaterial/p
		| archdesc/*/relatedmaterial/p
		| archdesc/separatedmaterial/p
		| archdesc/*/separatedmaterial/p
		| archdesc/relatedmaterial/note/p
		| archdesc/*/relatedmaterial/note/p
		| archdesc/separatedmaterial/note/p
		| archdesc/*/separatedmaterial/note/p">
		<p style="margin-left: 25pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>


<!--This template formats the top-level controlaccess element.
	It begins by testing to see if there is any controlled
	access element with content. It then invokes one of two templates
	for the children of controlaccess.  -->
	<xsl:template match="archdesc/controlaccess">
		<xsl:if test="string(child::*)">
        <h3 style="white-space: nowrap;">
				<xsl:element name="a">
                <xsl:attribute name="href">
                <xsl:text>javascript:changeDisplay('controlaccess');</xsl:text>
                </xsl:attribute>
                <img id="xcontrolaccess"
                src="expanded.png"
                alt="expand/collapse"/>
                </xsl:element>
                <a name="{generate-id(head)}">
                    Index Terms
                </a>
        </h3>
			<div id="controlaccess" class="showhide" style="display: block;">
				<xsl:apply-templates select="p | note/p"/>
			<xsl:choose>
				<!--Apply this template when there are recursive controlaccess
				elements.-->
				<xsl:when test="controlaccess">
					<xsl:apply-templates mode="recursive" select="."/>
				</xsl:when>
				<!--Apply this template when the controlled terms are entered
				directly under the controlaccess element.-->
				<xsl:otherwise>
					<xsl:apply-templates mode="direct" select="."/>
				</xsl:otherwise>
			</xsl:choose>
            </div>
		</xsl:if>
        <hr />
	</xsl:template>

<!--This template formats controlled terms that are entered
	directly under the controlaccess element.  Elements are alphabetized.-->
	<xsl:template mode="direct" match="archdesc/controlaccess">
		<xsl:for-each select="subject |corpname | famname | persname | genreform | title | geogname | occupation">
			<xsl:sort select="." data-type="text" order="ascending"/>
			<div style="margin-left:50pt">
				<xsl:apply-templates/>
			</div>
		</xsl:for-each>
		<hr> </hr>
	</xsl:template>
	
<!--When controlled terms are nested within recursive
	controlaccess elements, the template for controlaccess/controlaccess
	is applied.-->
	<xsl:template mode="recursive" match="archdesc/controlaccess">
			<xsl:apply-templates select="controlaccess"/>
		<hr> </hr>
	</xsl:template>

<!--This template formats controlled terms that are nested within recursive
	controlaccess elements.   Terms are alphabetized within each grouping.-->
	<xsl:template match="archdesc/controlaccess/controlaccess">
		<xsl:apply-templates select="head"/>
		
        <xsl:for-each select="subject |corpname | famname | persname | genreform | title | geogname | occupation">
			<xsl:sort select="." data-type="text" order="ascending"/>
			<div style="margin-left:50pt">
				<xsl:apply-templates/>
			</div>
		</xsl:for-each>
	</xsl:template>

	<!--This template rule formats a top-level access and use retriction elements.
	They are displayed under a common heading.
	It begins by testing to see if there are any restriction elements with content.-->
	<xsl:template name="archdesc-restrict">
		<xsl:if test="string(archdesc/userestrict/*)
		or string(archdesc/accessrestrict/*)
		or string(archdesc/*/userestrict/*)
		or string(archdesc/*/accessrestrict/*)">
			<h3>
				<a name="restrictlink">
					<b>
						<xsl:text>Restrictions</xsl:text>
					</b>
				</a>
			</h3>
			<xsl:apply-templates select="archdesc/accessrestrict
				| archdesc/*/accessrestrict"/>
			<xsl:apply-templates select="archdesc/userestrict
				| archdesc/*/userestrict"/>
			<hr></hr>
		</xsl:if>
	</xsl:template>

	<xsl:template match="archdesc/accessrestrict/head
	| archdesc/userestrict/head
	| archdesc/*/accessrestrict/head
	| archdesc/*/userestrict/head">
			<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="archdesc/accessrestrict/p
	| archdesc/userestrict/p
	| archdesc/*/accessrestrict/p
	| archdesc/*/userestrict/p
	| archdesc/accessrestrict/note/p
	| archdesc/userestrict/note/p
	| archdesc/*/accessrestrict/note/p
	| archdesc/*/userestrict/note/p">
		<p style="margin-left:50pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>




	<!--This templates consolidates all the other administrative information
	 elements into one block under a common heading.  It formats these elements
	 regardless of which of three encodings has been utilized.  They may be
	 children of archdesc, admininfo, or descgrp.
	 It begins by testing to see if there are any elements of this type
	 with content.-->
	 
	<xsl:template name="archdesc-admininfo">
		<xsl:if test="string(archdesc/admininfo/custodhist/*)
		or string(archdesc/altformavail/*)
		or string(archdesc/prefercite/*)
		or string(archdesc/acqinfo/*)
		or string(archdesc/processinfo/*)
		or string(archdesc/appraisal/*)
		or string(archdesc/accruals/*)
		or string(archdesc/*/custodhist/*)
		or string(archdesc/*/altformavail/*)
		or string(archdesc/*/prefercite/*)
		or string(archdesc/*/acqinfo/*)
		or string(archdesc/*/processinfo/*)
		or string(archdesc/*/appraisal/*)
		or string(archdesc/*/accruals/*)">
			<h3>
				<xsl:element name="a">
                <xsl:attribute name="href">
                <xsl:text>javascript:changeDisplay('admininfo');</xsl:text>
                </xsl:attribute>
                <img id="xadmininfo"
                src="expanded.png"
                alt="expand/collapse"/>
                </xsl:element>
			<a name="adminlink"><xsl:text>Administrative Information</xsl:text></a>
			</h3>
            <div id="admininfo" class="showhide" style="display: block;">
			<xsl:apply-templates select="archdesc/custodhist
				| archdesc/*/custodhist"/>
			<xsl:apply-templates select="archdesc/altformavail
				| archdesc/*/altformavail"/>
			<xsl:apply-templates select="archdesc/prefercite
				| archdesc/*/prefercite"/>
			<xsl:apply-templates select="archdesc/acqinfo
				| archdesc/*/acqinfo"/>
			<xsl:apply-templates select="archdesc/processinfo
				| archdesc/*/processinfo"/>
			<xsl:apply-templates select="archdesc/admininfo/appraisal
				| archdesc/*/appraisal"/>
			<xsl:apply-templates select="archdesc/admininfo/accruals
				| archdesc/*/accruals"/>
            </div>
			<hr></hr>
		</xsl:if>
	</xsl:template>
	

	<!--This template rule formats the head element of top-level elements of
	administrative information.-->
		<xsl:template match="custodhist/head
		| archdesc/altformavail/head
		| archdesc/prefercite/head
		| archdesc/acqinfo/head
		| archdesc/processinfo/head
		| archdesc/appraisal/head
		| archdesc/accruals/head
		| archdesc/*/custodhist/head
		| archdesc/*/altformavail/head
		| archdesc/*/prefercite/head
		| archdesc/*/acqinfo/head
		| archdesc/*/processinfo/head
		| archdesc/*/appraisal/head
		| archdesc/*/accruals/head">
			<a name="{generate-id()}">
				<b>
					<xsl:apply-templates/>
				</b>
			</a>
	</xsl:template>	
		
	<xsl:template match="custodhist/p
		| archdesc/altformavail/p
		| archdesc/prefercite/p
		| archdesc/acqinfo/p
		| archdesc/processinfo/p
		| archdesc/appraisal/p
		| archdesc/accruals/p
		| archdesc/*/custodhist/p
		| archdesc/*/altformavail/p
		| archdesc/*/prefercite/p
		| archdesc/*/acqinfo/p
		| archdesc/*/processinfo/p
		| archdesc/*/appraisal/p
		| archdesc/*/accruals/p
		| archdesc/custodhist/note/p
		| archdesc/altformavail/note/p
		| archdesc/prefercite/note/p
		| archdesc/acqinfo/note/p
		| archdesc/processinfo/note/p
		| archdesc/appraisal/note/p
		| archdesc/accruals/note/p
		| archdesc/*/custodhist/note/p
		| archdesc/*/altformavail/note/p
		| archdesc/*/prefercite/note/p
		| archdesc/*/acqinfo/note/p
		| archdesc/*/processinfo/note/p
		| archdesc/*/appraisal/note/p
		| archdesc/*/accruals/note/p">
		
		<p style="margin-left:50pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
		
	<xsl:template match="archdesc/otherfindaid
		| archdesc/*/otherfindaid
		| archdesc/bibliography
		| archdesc/*/bibliography
		| archdesc/originalsloc
		| archdesc/phystech">
			<xsl:apply-templates/>
			<hr></hr>
		</xsl:template>
		
	<xsl:template match="archdesc/otherfindaid/head
		| archdesc/*/otherfindaid/head
		| archdesc/bibliography/head
		| archdesc/*/bibliography/head
		| archdesc/fileplan/head
		| archdesc/*/fileplan/head
		| archdesc/phystech/head
		| archdesc/originalsloc/head">
		<h3>
			<a name="{generate-id()}">
				<b>
					<xsl:apply-templates/>
				</b>
			</a>
		</h3>
	</xsl:template>

	<xsl:template match="archdesc/otherfindaid/p
		| archdesc/*/otherfindaid/p
		| archdesc/bibliography/p
		| archdesc/*/bibliography/p
		| archdesc/otherfindaid/note/p
		| archdesc/*/otherfindaid/note/p
		| archdesc/bibliography/note/p
		| archdesc/*/bibliography/note/p
		| archdesc/fileplan/p
		| archdesc/*/fileplan/p
		| archdesc/fileplan/note/p
		| archdesc/*/fileplan/note/p
		| archdesc/phystech/p
		| archdesc/phystechc/note/p
		| archdesc/originalsloc/p
		| archdesc/originalsloc/note/p">
		<p style="margin-left:25pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>

<!--This template rule tests for and formats the top-level index element. It begins
	by testing to see if there is an index element with content.-->
	<xsl:template match="archdesc/index
		| archdesc/*/index">
			<table width="100%">
				<tr>
					<td width="5%"> </td>
					<td width="45%"> </td>
					<td width="50%"> </td>
				</tr>
				<tr>
					<td colspan="3">
						<h3>
							<a name="{generate-id(head)}">
								<b>
									<xsl:apply-templates select="head"/>
								</b>
							</a>
						</h3>
					</td>
				</tr>
				<xsl:for-each select="p | note/p">
					<tr>
						<td></td>
						<td colspan="2">
							<xsl:apply-templates/>
						</td>
					</tr>
				</xsl:for-each>

				<!--Processes each index entry.-->
				<xsl:for-each select="indexentry">

				<!--Sorts each entry term.-->
					<xsl:sort select="corpname | famname | function | genreform | geogname | name | occupation | persname | subject"/>
					<tr>
						<td></td>
						<td>
							<xsl:apply-templates select="corpname | famname | function | genreform | geogname | name | occupation | persname | subject"/>
						</td>
						<!--Supplies whitespace and punctuation if there is a pointer
						group with multiple entries.-->

						<xsl:choose>
							<xsl:when test="ptrgrp">
								<td>
									<xsl:for-each select="ptrgrp">
										<xsl:for-each select="ref | ptr">
											<xsl:apply-templates/>
											<xsl:if test="preceding-sibling::ref or preceding-sibling::ptr">
												<xsl:text>, </xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
								</td>
							</xsl:when>
							<!--If there is no pointer group, process each reference or pointer.-->
							<xsl:otherwise>
								<td>
									<xsl:for-each select="ref | ptr">
										<xsl:apply-templates/>
									</xsl:for-each>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
					<!--Closes the indexentry.-->
				</xsl:for-each>
			</table>
			<hr></hr>
	</xsl:template>
    
<!-- .................Section 1.................. -->

<!--This section of the stylesheet formats dsc, its head, and
any introductory paragraphs.-->

	<xsl:template match="archdesc/dsc">
			<xsl:apply-templates/>
	</xsl:template>
	
	<!--Formats dsc/head and makes it a link target.-->
	<xsl:template match="dsc/head">
		<h3>
				<xsl:element name="a">
                <xsl:attribute name="href">
                <xsl:text>javascript:changeDisplay('</xsl:text>
                <xsl:value-of select="generate-id(current())"/>
                <xsl:text>');</xsl:text>
                </xsl:attribute>
                <img id="x{generate-id(current())}"
                src="expanded.png"
                alt="expand/collapse"/>
                </xsl:element>
			<a name="{generate-id()}">
				<xsl:apply-templates/>
			</a>
		</h3>
	</xsl:template>
	
	<xsl:template match="dsc/p | dsc/note/p">
		<p style="margin-left:25pt">
			<xsl:apply-templates/>
		</p>
	</xsl:template>

        <!--Insert the address for the dsc stylesheet of your choice here.-->
        <xsl:include href="dsc6.xsl"/>
</xsl:stylesheet>
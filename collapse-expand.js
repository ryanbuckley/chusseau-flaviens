//define an array identifying all expandible/collapsible divs by ID
//this is limited to div elements.
var divs=document.getElementsByTagName('div');
var ids=new Array();
var n=0;
for (var i=0;i<divs.length;i++){
//conditional statement limits divs in array to those with class 'showhide'
//change 'showhide' to whatever class identifies expandable/collapsible divs.
if (divs[i].className=='showhide') {
ids[n]=divs[i].id;
n++;
}
}
function expandAll(){
//loop through the array and expand each element by ID
for (var i=0;i<ids.length;i++){
var ex_obj = document.getElementById(ids[i]).style;
ex_obj.display = ex_obj.display == "none" ? "block" : "block";
//swap corresponding arrow image to reflect change
var arrow = document.getElementById("x"+(ids[i]));
arrow.src="expanded.png";
}
}
function collapseAll(){
//loop through the array and collapse each element by ID
for (var i=0;i<ids.length;i++){
var col_obj = document.getElementById(ids[i]).style;
col_obj.display = col_obj.display == "block" ? "none" : "none";
//swap corresponding arrow image to reflect change
var arrow = document.getElementById("x"+(ids[i]));
arrow.src="collapsed.png";
}
}
// show/hide script for individual div toggling
//by Adrienne MacKay, adapted 2008 by Stephanie Adamson to include arrow
//image swapping
function changeDisplay(obj_name) {
var my_obj = document.getElementById(obj_name);
var arrow=document.getElementById("x"+obj_name);
if (my_obj.style.display=="none") {
my_obj.style.display="block";// JavaScript Document
arrow.src="expanded.png";
}
else { my_obj.style.display="none";
arrow.src="collapsed.png";
}
}

<!-- EAD Cookbook Stylesheet 3    Version 0.92   2002 March 19  -->
<!--  This stylesheet uses Style 3 to generate a frame presentation using the xt:document extension in XT. Four separate files are output: a basic frameset, two table of contents pages and the body of the fnding aid.-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xt="http://www.jclark.com/xt" extension-element-prefixes="xt" version="1.0">

<xsl:strip-space elements="*"/>

<xsl:template match="/">

<!-- Creates a variable equal to the value of the number in eadid which serves as the base
 for file names for the various components of the frameset.-->
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>

<!-- The following four xt:document statements create the four output files that comprise the 
frameset.-->

<!-- Creates the basic frameset.-->
<xt:document method="html" href="{$file}f.html">
   <xsl:call-template name="frameset"/>
</xt:document>

<!-- Creates the basic table of contents.--> 
<xt:document method="html" href="{$file}t.html">
   <xsl:call-template name="toc"/>
</xt:document>

<!-- Creates the body of the finding aid.--> 
<xt:document method="html" href="{$file}b.html">
   <xsl:call-template name="body"/>
</xt:document>

<!-- Creates a table of contents with the series titles enumerated.-->
<xt:document method="html" href="{$file}p.html">
   <xsl:call-template name="plus"/>
</xt:document>

</xsl:template>

<!-- Defines the frameset.-->
<xsl:template name="frameset">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<title>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/titleproper"/>
<xsl:text>  </xsl:text>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/subtitle"/>
</title>

<xsl:element name="meta">
<xsl:attribute name="name">dc.title</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/titleproper"/>
<xsl:text>  </xsl:text>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/subtitle"/>
</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.author</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/archdesc/did/origination"/>
</xsl:attribute>
</xsl:element>

<xsl:for-each select="ead//controlaccess/persname | ead//controlaccess/corpname"> 
<xsl:choose>
<xsl:when test="@encodinganalog='600'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:when test="@encodinganalog='610'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:when test="@encodinganalog='611'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:when test="@encodinganalog='700'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:when test="@encodinganalog='710'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:otherwise>
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="ead//controlaccess/subject">
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:for-each>

<xsl:element name="meta">
<xsl:attribute name="name">dc.title</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/archdesc/did/unittitle"/>
</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.type</xsl:attribute>
<xsl:attribute name="content">text</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.format</xsl:attribute>
<xsl:attribute name="content">manuscripts</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.format</xsl:attribute>
<xsl:attribute name="content">finding aids</xsl:attribute>
</xsl:element>

<xsl:for-each select="ead//controlaccess/geogname">
<xsl:element name="meta">
<xsl:attribute name="name">dc.coverage</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:for-each>

</head>
<frameset cols="25%,75%">
<frame name="toc" src="{$file}t.html">
</frame>
<frame name="body" src="{$file}b.html">
</frame>
</frameset>
</html>
</xsl:template>

<!-- Defines the base table of contents.-->
<xsl:template name="toc">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<base target="body"/>
</head>
<body style="font-family:arial" bgcolor="#E0E0E0">

<h4>TABLE OF CONTENTS</h4>

<!-- A series of tests determine which elements will be included in the table of contents.-->
<xsl:if test="ead/archdesc/did">
<p><b><a href="{$file}b.html#a1">
<xsl:value-of select="ead/archdesc/did/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<b>
<a href="{$file}b.html#a2">
<xsl:value-of select="ead/archdesc/bioghist/head"/>
</a>
</b></p>
</xsl:if>

<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a3">
<xsl:value-of select="ead/archdesc/scopecontent/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/organization[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a4">
<xsl:value-of select="ead/archdesc/organization/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a5">
<xsl:value-of select="ead/archdesc/arrangement/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/admininfo/userestrict[string-length(text()|*)!=0] | ead/archdesc/admininfo/accessrestrict[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a14">
<xsl:text>Restrictions</xsl:text>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a12">
<xsl:value-of select="ead/archdesc/controlaccess/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/relatedmaterial[string-length(text()|*)!=0] | ead/archdesc/add/separatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a6">
Related Material
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/admininfo/acqinfo[string-length(text()|*)!=0] | ead/archdesc/admininfo/processinfo[string-length(text()|*)!=0] |  ead/archdesc/admininfo/prefercite[string-length(text()|*)!=0] | ead/archdesc/admininfo/custodialhist[string-length(text()|*)!=0] | ead/archdesc/admininfo/altformavailable | ead/archdesc/admininfo/appraisal | ead/archdesc/admininfo/accruals[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a13">
Administrative Information
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/otherfindaid[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b><a href="{$file}b.html#a8">
<xsl:value-of select="ead/archdesc/add/otherfindaid/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a11">
<xsl:value-of select="ead/archdesc/odd/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/bibliography[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a10">
<xsl:value-of select="ead/archdesc/add/bibliography/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/index[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a9">
<xsl:value-of select="ead/archdesc/add/index/head"/>
</a></b></p>
</xsl:if>


<xsl:if test="ead/archdesc/dsc[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<a href="{$file}p.html#a45" target="toc">
<img src="plus.gif" border="0"/>
</a>
<a name="a46">
</a>
<b>
<a href="{$file}b.html#a23">
<xsl:text> </xsl:text>
<xsl:value-of select="ead/archdesc/dsc/head"/>
</a>
</b>
</p>
</xsl:if>

</body>
</html>
</xsl:template>

<!-- Creates a second table of contents with the series titles enumerated.-->
<xsl:template name="plus">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<base target="body"/>
</head>
<body style="font-family:arial" bgcolor="#E0E0E0">


<h4>TABLE OF CONTENTS</h4>

<!-- A series of tests determine which elements will be included in the table of contents with series titles enumerated.-->

<xsl:if test="ead/archdesc/did">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a1">
<xsl:value-of select="ead/archdesc/did/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a2">
<xsl:value-of select="ead/archdesc/bioghist/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a3">
<xsl:value-of select="ead/archdesc/scopecontent/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/organization[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a4">
<xsl:value-of select="ead/archdesc/organization/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a5">
<xsl:value-of select="ead/archdesc/arrangement/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/admininfo/userestrict[string-length(text()|*)!=0] | ead/archdesc/admininfo/accessrestrict[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a14">
<xsl:text>Restrictions</xsl:text>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a12">
<xsl:value-of select="ead/archdesc/controlaccess/head"/>
</a></b></p>
</xsl:if>
     
<xsl:if test="ead/archdesc/add/relatedmaterial[string-length(text()|*)!=0] | ead/archdesc/add/separatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a6">
Related Material
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/admininfo/acqinfo[string-length(text()|*)!=0] | ead/archdesc/admininfo/processinfo[string-length(text()|*)!=0] | ead/archdesc/admininfo/prefercite[string-length(text()|*)!=0] | ead/archdesc/admininfo/custodialhist[string-length(text()|*)!=0] | ead/archdesc/admininfo/altformavailable[string-length(text()|*)!=0] | ead/archdesc/admininfo/appraisal[string-length(text()|*)!=0] |  ead/archdesc/admininfo/accruals[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a13">
Administrative Information
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/otherfindaid[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a8">
<xsl:value-of select="ead/archdesc/add/otherfindaid/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a11">
<xsl:value-of select="ead/archdesc/odd/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/bibliography[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a10">
<xsl:value-of select="ead/archdesc/add/bibliography/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/add/index[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<b><a href="{$file}b.html#a9">
<xsl:value-of select="ead/archdesc/add/index/head"/>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/dsc[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<a href="{$file}t.html#a46" target="toc">
<img src="minus.gif" border="0"/>
</a>
<a name="a45"/>
<b>
<a href="{$file}b.html#a23">
<xsl:text> </xsl:text>
<xsl:value-of select="ead/archdesc/dsc/head"/>
</a></b></p>

<!-- Displays the title and date of each series and numbers
 them to form a hyperlink to the base document.-->
<xsl:for-each select="ead/archdesc/dsc/c01">
<p style="margin-left:10pt; margin-top:-5pt">
<font size="-1">
<a>
<xsl:attribute name="href">
<xsl:value-of select="$file"/>
b.html#series<xsl:number value="position()" format="1"/>
</xsl:attribute>
<xsl:choose>
<xsl:when test="did/unittitle/unitdate">
<xsl:for-each select="did/unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="did/unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="did/unitdate"/>
</xsl:otherwise>
</xsl:choose>
</a>
</font></p>
</xsl:for-each>
</xsl:if>
</body>
</html>
</xsl:template>

<!-- Creates the body of the finding aid.-->
<xsl:template name="body">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<title>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt"/>
</title>
<style>
h1, h2, h3, h4 {font-family: arial} 
</style>
</head>

<body bgcolor="#FAFDD5">

<xsl:call-template name="eadheader"/>
<xsl:call-template name="archdesc-did"/>
<xsl:call-template name="archdesc-bioghist"/>
<xsl:call-template name="archdesc-scopecontent"/>
<xsl:call-template name="archdesc-organization"/>
<xsl:call-template name="archdesc-arrangement"/>
<xsl:call-template name="archdesc-restrict"/>
<xsl:call-template name="archdesc-control"/>
<xsl:call-template name="archdesc-relatedmaterial"/>
<xsl:call-template name="archdesc-admininfo"/>
<xsl:call-template name="archdesc-otherfindaid"/>
<xsl:call-template name="archdesc-index"/>
<xsl:call-template name="archdesc-odd"/>
<xsl:call-template name="archdesc-bibliography"/>
<xsl:call-template name="dsc"/>
</body>
</html>
</xsl:template>

<xsl:template name="eadheader">
<xsl:for-each select="ead/eadheader/filedesc/titlestmt">
<center><a name="a0"><img src="yourlogo.gif"/></a></center>
<br/>
<h2><center>
<xsl:value-of select="titleproper"/>
</center></h2>
<h3><center>
<xsl:value-of select="subtitle"/>
</center></h3>
<br/>
</xsl:for-each>
<hr/>
</xsl:template>

<!-- The following templates format the display of various RENDER attributes.-->


<xsl:template match="emph[@render='bold']">
	<b>
		<xsl:apply-templates/>
	</b>
</xsl:template>

<xsl:template match="emph[@render='italic']">
	<i>
		<xsl:apply-templates/>
	</i>
</xsl:template>

<xsl:template match="emph[@render='underline']">
	<u>
		<xsl:apply-templates/>
	</u>
</xsl:template>

<xsl:template match="emph[@render='sub']">
	<sub>
		<xsl:apply-templates/>
	</sub>
</xsl:template>

<xsl:template match="emph[@render='super']">
	<super>
		<xsl:apply-templates/>
	</super>
</xsl:template>

<xsl:template match="emph[@render='quoted']">
	<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
	<xsl:text>"</xsl:text>
</xsl:template>

<xsl:template match="emph[@render='boldquoted']">
	<b>
		<xsl:text>"</xsl:text>
			<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</b>
</xsl:template>

<xsl:template match="emph[@render='boldunderline']">
	<b>
		<u>
			<xsl:apply-templates/>
		</u>
	</b>
</xsl:template>

<xsl:template match="emph[@render='bolditalic']">
	<b>
		<i>
			<xsl:apply-templates/>
		</i>
	</b>
</xsl:template>

<xsl:template match="emph[@render='boldsmcaps']">
	<font style="font-variant: small-caps">
		<b>
			<xsl:apply-templates/>
		</b>
	</font>
</xsl:template>

<xsl:template match="emph[@render='smcaps']">
	<font style="font-variant: small-caps">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="title[@render='bold']">
	<b>
		<xsl:apply-templates/>
	</b>
</xsl:template>

<xsl:template match="title[@render='italic']">
	<i>
		<xsl:apply-templates/>
	</i>
</xsl:template>

<xsl:template match="title[@render='underline']">
	<u>
		<xsl:apply-templates/>
	</u>
</xsl:template>

<xsl:template match="title[@render='sub']">
	<sub>
		<xsl:apply-templates/>
	</sub>
</xsl:template>

<xsl:template match="title[@render='super']">
	<super>
		<xsl:apply-templates/>
	</super>
</xsl:template>

<xsl:template match="title[@render='quoted']">
	<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
	<xsl:text>"</xsl:text>
</xsl:template>

<xsl:template match="title[@render='boldquoted']">
	<b>
		<xsl:text>"</xsl:text>
			<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</b>
</xsl:template>

<xsl:template match="title[@render='boldunderline']">
	<b>
		<u>
			<xsl:apply-templates/>
		</u>
	</b>
</xsl:template>

<xsl:template match="title[@render='bolditalic']">
	<b>
		<i>
			<xsl:apply-templates/>
		</i>
	</b>
</xsl:template>

<xsl:template match="title[@render='boldsmcaps']">
	<font style="font-variant: small-caps">
		<b>
			<xsl:apply-templates/>
		</b>
	</font>
</xsl:template>

<xsl:template match="title[@render='smcaps']">
	<font style="font-variant: small-caps">
		<xsl:apply-templates/>
	</font>
</xsl:template>


<!-- This template converts a Ref element into an HTML anchor.-->

<xsl:template match="ead/archdesc//ref">
<xsl:variable name="target">
<xsl:value-of select="@target"/>
</xsl:variable>
<a href="#{$target}">
<xsl:value-of select="."/>
</a>
</xsl:template>


<!--This template rule formats a list element.-->
<xsl:template match="*/list">
<xsl:for-each select="item">
<p style="margin-left: 60pt">
<xsl:apply-templates/>
</p>
</xsl:for-each>
</xsl:template>

<!--Formats a simple table. The width of each column is defined by the colwidth attribute in a colspec element.-->
<xsl:template match="*/table">
<xsl:for-each select="tgroup">
<table width="100%">
<tr>
<xsl:for-each select="colspec">
<td width="{@colwidth}"/>
</xsl:for-each>
</tr>
<xsl:for-each select="thead">
<xsl:for-each select="row">
<tr>
<xsl:for-each select="entry">
<td valign="top"><b><xsl:value-of select="."/></b>
</td>
</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="tbody">
<xsl:for-each select="row">
<tr>
<xsl:for-each select="entry">
<td valign="top"><xsl:value-of select="."/></td>
</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
</xsl:template>


<!--This template rule formats the top-level did element.-->
<xsl:template name="archdesc-did">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>


<!--For each element of the did, this template inserts the value of the LABEL attribute or, if none is present, a default value.-->

<xsl:for-each select="ead/archdesc/did">
<table width="100%">
<tr><td width="5%"> </td><td width="20%"> </td>
<td width="75"> </td></tr>
<tr><td colspan="3"><h3><a name="a1">
<xsl:apply-templates select="head"/>
</a></h3> </td></tr>

<xsl:if test="origination[string-length(text()|*)!=0]">
<xsl:for-each select="origination">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top"><b>
<xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Creator: </xsl:text></b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>

<!-- Tests for and processes various permutations of unittitle and unitdate.-->
<xsl:for-each select="unittitle">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top"><b>
<xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top"><b>
<xsl:text>Title: </xsl:text>
</b></td><td>
<xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>

<xsl:if test="child::unitdate">
<xsl:choose>
<xsl:when test="./unitdate/@label">
<tr><td> </td><td valign="top">
<b>
<xsl:value-of select="./unitdate/@label"/>
</b></td><td>
<xsl:apply-templates select="./unitdate"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b>
<xsl:text>Dates: </xsl:text>
</b></td><td>
<xsl:apply-templates select="./unitdate"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>
</xsl:for-each>

<!-- Processes the unit date if it is not a child of unit title but a child of did, the current context.-->
<xsl:if test="unitdate">
<xsl:for-each select="unitdate[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="./@label">
<tr><td> </td><td valign="top">
<b>
<xsl:value-of select="./@label"/>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b>
<xsl:text>Dates: </xsl:text>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>

<xsl:if test="abstract[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="abstract"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Abstract: </xsl:text></b></td><td>
<xsl:apply-templates select="abstract"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<xsl:if test="physdesc[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="physdesc"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Quantity: </xsl:text></b></td><td>
<xsl:apply-templates select="physdesc"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<xsl:if test="unitid[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="unitid"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Identification: </xsl:text></b></td><td>
<xsl:apply-templates select="unitid"/>

</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<xsl:if test="physloc[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="physloc"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Location: </xsl:text></b></td><td>
<xsl:apply-templates select="physloc"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<xsl:if test="note[string-length(text()|*)!=0]">
<xsl:for-each select="note">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td></tr>
<xsl:for-each select="p">
<tr><td> </td><td valign="top">
<xsl:apply-templates/>
</td></tr>
</xsl:for-each>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b>Location:</b></td><td>
<xsl:apply-templates select="note"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>
             
</table>
<hr/>

</xsl:for-each>
</xsl:template>

<!--This template rule formats the top-level bioghist element.-->
<xsl:template name="archdesc-bioghist">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/bioghist">
<xsl:apply-templates/>
<hr/>
</xsl:for-each>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/head">
<h3><a name="a2">
<xsl:apply-templates/>
</a></h3>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/p">
<p style="margin-left: 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/chronlist">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/bioghist">
<h3>
<xsl:apply-templates select="head"/>
</h3>
<xsl:for-each select="p">
<p style="margin-left: 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:template>

<!--This template rule formats a chronlist element.-->
<xsl:template match="*/chronlist">
<table width="100%">
<tr><td width="5%"> </td><td width="30%"> </td>
<td width="65%"> </td></tr>

<xsl:for-each select="listhead">
<tr><td><b>
<xsl:apply-templates select="head01"/>
</b></td>
<td><b>
<xsl:apply-templates select="head02"/>
</b></td></tr>
</xsl:for-each>

<xsl:for-each select="chronitem">
<tr><td/><td valign="top">
<xsl:apply-templates select="date"/>
</td>
<td valign="top">
<xsl:apply-templates select="event"/>
</td></tr>
</xsl:for-each>
</table>
</xsl:template>



<!--This template rule formats the scopecontent element.-->
<xsl:template name="archdesc-scopecontent">
<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/scopecontent"> 
<xsl:apply-templates/>
</xsl:for-each>
<hr/>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/scopecontent/head">
<h3><a name="a3">
<xsl:apply-templates/>
</a></h3>
</xsl:template>

<!-- This formats an organization list embedded in a scope content statement.-->
<xsl:template match="ead/archdesc/scopecontent/organization">
<xsl:for-each select="p">
<p style="margin-left: 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
<xsl:for-each select="list">
<xsl:for-each select="item">
<p style="margin-left: 60pt">
<a><xsl:attribute name="href">#series<xsl:number/>
</xsl:attribute>
<xsl:apply-templates select="."/>
</a>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:template>

<xsl:template match="ead/archdesc/scopecontent/p">
<p style="margin-left: 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>



<!--This template rule formats the organization element.-->
<xsl:template name="archdesc-organization">
<xsl:if test="ead/archdesc/organization[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/organization">
<table width="100%">
<tr><td width="5%"> </td><td width="5%"> </td>
<td width="90%"> </td></tr>

<tr><td colspan="3"> <h3><a name="a4">
<xsl:apply-templates select="head"/>
</a></h3>
</td></tr>

<xsl:for-each select="p">
<tr><td> </td><td colspan="2">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>

<xsl:for-each select="list">
<tr><td> </td><td colspan="2">
<xsl:apply-templates select="head"/>
</td></tr>
<xsl:for-each select="item">
<tr><td> </td><td> </td><td colspan="1">
<a><xsl:attribute name="href">#series<xsl:number/>
</xsl:attribute>
<xsl:apply-templates select="."/>
</a>
</td></tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level arrangement element.-->
<xsl:template name="archdesc-arrangement">
<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/arrangement">
<h3><a name="a5">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level relatedmaterial element.-->
<xsl:template name="archdesc-relatedmaterial">
<xsl:if test="ead/archdesc/add/relatedmaterial[string-length(text()|*)!=0] | ead/archdesc/add/separatedmaterial[string-length(text()|*)!=0]">
<h3><a name="a6">
<b><xsl:text>Related Material</xsl:text>
</b></a></h3>
<xsl:for-each select="ead/archdesc/add/relatedmaterial | ead/archdesc/add/separatedmaterial">
<xsl:apply-templates select="*[not(self::head)]"/> 
</xsl:for-each>
<hr/>
</xsl:if>
</xsl:template>


<xsl:template match="ead/archdesc/add/relatedmaterial/p  | ead/archdesc/add/separatedmaterial/p">
<p style="margin-left : 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>


<!--This template rule formats the top-level otherfindaid element.-->
<xsl:template name="archdesc-otherfindaid">
<xsl:if test="ead/archdesc/add/otherfindaid[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/add/otherfindaid">
<h3><a name="a8">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
<p>

</p>
</xsl:for-each>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level index element.-->
<xsl:template name="archdesc-index">
<xsl:if test="ead/archdesc/add/index[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/add/index">
<h3><a name="a9">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level bibliography element.-->
<xsl:template name="archdesc-bibliography">
<xsl:if test="ead/archdesc/add/bibliography[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/add/bibliography">
<h3><a name="a10">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<xsl:for-each select="bibref">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
<hr/>
</xsl:if>
</xsl:template>


<!--This template rule formats the top-level odd element.-->
<xsl:template name="archdesc-odd">
<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/odd">
<h3><a name="a11">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt"> 
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>


<xsl:template name="archdesc-control">
<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/controlaccess">
<table width="100%">
<tr><td width="5%"> </td><td width="5%"> </td>
<td width="90%"> </td></tr>

<tr><td colspan="3"><h3><a name="a12">
<xsl:apply-templates select="head"/>
</a></h3> </td></tr>

<tr><td> </td><td colspan="2">
<xsl:apply-templates select="p"/>
</td></tr>
    
<xsl:for-each select="./controlaccess">
<tr><td> </td><td colspan="2"><b>
<xsl:apply-templates select="head"/>
</b></td></tr>

<xsl:for-each select="subject |corpname | persname | genreform | title | geogname | occupation">
<xsl:sort select="."/>
<tr><td/><td/><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level accessretrict element.-->
<xsl:template name="archdesc-restrict">
<xsl:if test="ead/archdesc/admininfo/accessrestrict[string-length(text()|*)!=0] | ead/archdesc/admininfo/userestrict[string-length(text()|*)!=0]">
<h3>
<a name="a14">
<b><xsl:text>Restrictions</xsl:text>
</b></a></h3>
<xsl:for-each select="ead/archdesc/admininfo/accessrestrict">
<h4 style="margin-left : 15pt"><b><xsl:value-of select="head"/></b></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/admininfo/userestrict">
<h4 style="margin-left : 15pt"><b><xsl:value-of select="head"/></b></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>


<xsl:template name="archdesc-admininfo">
<xsl:if test="ead/archdesc/admininfo/prefercite[string-length(text()|*)!=0] | ead/archdesc/admininfo/custodhist[string-length(text()|*)!=0] |  ead/archdesc/admininfo/altformavail[string-length(text()|*)!=0] | ead/archdesc/admininfo/acqinfo[string-length(text()|*)!=0] |  ead/archdesc/admininfo/processinfo[string-length(text()|*)!=0] | ead/archdesc/admininfo/appraisal[string-length(text()|*)!=0] |  ead/archdesc/admininfo/accruals[string-length(text()|*)!=0]">
<h3><a name="a13">
<xsl:text>Administrative Information</xsl:text>
</a></h3>
<xsl:call-template name="archdesc-custodhist"/>
<xsl:call-template name="archdesc-altform"/>
<xsl:call-template name="archdesc-prefercite"/>
<xsl:call-template name="archdesc-acqinfo"/>
<xsl:call-template name="archdesc-processinfo"/>
<xsl:call-template name="archdesc-appraisal"/>
<xsl:call-template name="archdesc-accruals"/>
<p>

</p>
<hr/>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level custodhist element.-->
<xsl:template name="archdesc-custodhist">
<xsl:if test="ead/archdesc/admininfo/custodhist[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/custodhist">
<h4 style="margin-left : 15pt">
<a name="a16">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--This template rule formats a top-level altformavailable element.-->
<xsl:template name="archdesc-altform">
<xsl:if test="ead/archdesc/admininfo/altformavailable[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/altformavailable">
<h4 style="margin-left : 15pt">
<a name="a17">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>



<!--This template rule formats a top-level prefercite element.-->
<xsl:template name="archdesc-prefercite">
<xsl:if test="ead/archdesc/admininfo/prefercite[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/prefercite">
<h4 style="margin-left : 15pt">
<a name="a18">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--This template rule formats a top-level acqinfo element.-->
<xsl:template name="archdesc-acqinfo">
<xsl:if test="ead/archdesc/admininfo/acqinfo[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/acqinfo">
<h4 style="margin-left : 15pt"> 
<a name="a19">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level procinfo element.-->
<xsl:template name="archdesc-processinfo">
<xsl:if test="ead/archdesc/admininfo/processinfo[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/processinfo">
<h4 style="margin-left : 15pt">
<a name="a20">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level appraisal element.-->
<xsl:template name="archdesc-appraisal">
<xsl:if test="ead/archdesc/admininfo/appraisal[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/appraisal">
<h4 style="margin-left : 15pt"> 
<a name="a21">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level accruals element.-->
<xsl:template name="archdesc-accruals">
<xsl:if test="ead/archdesc/admininfo/accruals[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/admininfo/accruals">
<h4 style="margin-left : 15pt">
<a name="a22">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 25pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<xsl:template name="dsc">
<xsl:for-each select="ead/archdesc/dsc">
<h3><a name="a23">
<xsl:apply-templates select="head"/>
</a></h3>

<p style="margin-left: 25 pt"><i>
<xsl:apply-templates select="p"/>
</i></p>



<xsl:choose>

<!--This stylesheet supports four types of container markup: box and folder numbers, box numbers only, folder numbers only and combined box-folder numbers.  It also supports dsc elements with no containers specified.-->


<!-- Selects cases where only box numbers are given.-->
<xsl:when test=".//did/child::container[@type='box'] and not(.//did/child::container[@type='folder'])">
<xsl:call-template name="components-box-only"/>
</xsl:when>

<!-- Selects cases where both box and folder numbers are given.-->
<xsl:when test=".//did/child::container[@type='folder'] and .//did/child::container[@type='box']">
<xsl:call-template name="components-boxplusfolder"/>
</xsl:when>

<!-- Selects cases where only folder numbers are given.-->
<xsl:when test=".//did/child::container[@type='folder'] and not(.//did/child::container[@type='box'])">
<xsl:call-template name="components-folder-only"/>
</xsl:when>

<!-- Selects cases where box-folder numbers are given.-->
<xsl:when test=".//did/container[@type='box-folder']">
<xsl:call-template name="components-box-folder"/>
</xsl:when>

<!-- Selects cases where no container numbers are given.-->
<xsl:when test="./c01/did[not(child::container)] and ./c01//did[not(child::container)]">
<xsl:call-template name="no-container"/>
</xsl:when>
</xsl:choose>

</xsl:for-each>
</xsl:template>


<!-- A named template that formats components whose container elements only consist of box numbers.-->
<xsl:template name="components-box-only">

<!-- Process each c01.-->
<xsl:for-each select="c01">

<table width="100%">
<tr><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
</tr>
<a><xsl:attribute name="name">series<xsl:number/>
</xsl:attribute>
</a>

<!-- Processes each c01/did.  Box and folder numbers are never associated with c01 in this stylesheet so no testing for their appearance is necessary.-->

<xsl:for-each select="did">
<tr>
<td colspan="14"><h3>
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</h3>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>


<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>


                                  
<!-- Proceses each c02.-->
<xsl:for-each select="c02">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c02/did[container[@type='box']=$box-number]    or ../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c02-box-only"/>
</xsl:when>

<!-- If it did appear before, hide it here.-->
<xsl:otherwise>
<xsl:call-template name="hidebox-c02-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td colspan="12" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c03.-->
<xsl:for-each select="c03">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c03//did[container[@type='box']=$box-number]     or ../parent::c02/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c03-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c03-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c03 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td colspan="11" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c04.-->
<xsl:for-each select="c04">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../parent::c03/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c03//did[container[@type='box']=$box-number]     or ../../parent::c02/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">


<xsl:call-template name="showbox-c04-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c04-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c04 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c05-->
<xsl:for-each select="c05">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c05/did[container[@type='box']=$box-number]     or ../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../parent::c04/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../parent::c03/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c05-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c05-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c05 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c06-->
<xsl:for-each select="c06">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>


<xsl:choose>

<xsl:when test="not(../preceding-sibling::c06//did[container[@type='box']=$box-number]     or ../parent::c05/did[container[@type='box']=$box-number]      or ../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../parent::c04/did[container[@type='box']=$box-number]    or ../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">



<xsl:call-template name="showbox-c06-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c06-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c06 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c07.-->
<xsl:for-each select="c07">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c07//did[container[@type='box']=$box-number]     or ../parent::c06/did[container[@type='box']=$box-number]     or ../../preceding-sibling::c06//did[container[@type='box']=$box-number]      or ../../parent::c05/did[container[@type='box']=$box-number]      or ../../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../../parent::c04/did[container[@type='box']=$box-number]    or ../../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

   

<xsl:call-template name="showbox-c07-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c07-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c07 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c08.-->
<xsl:for-each select="c08">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c08//did[container[@type='box']=$box-number]     or ../parent::c07/did[container[@type='box']=$box-number]     or ../../preceding-sibling::c07//did[container[@type='box']=$box-number]     or ../../parent::c06/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c06//did[container[@type='box']=$box-number]      or ../../../parent::c05/did[container[@type='box']=$box-number]      or ../../../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../../../parent::c04/did[container[@type='box']=$box-number]    or ../../../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

   

<xsl:call-template name="showbox-c08-box-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c08-box-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c08 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</table>
<hr/>
<br/>
<br/>

</xsl:for-each>

</xsl:template>
<!-- Shows the box and folder numbers for a c02.-->
<xsl:template name="showbox-c02-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>
<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c02.-->
<xsl:template name="hidebox-c02-box-only">
<tr>
<td valign="top"/>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc"><xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c03.-->
<xsl:template name="showbox-c03-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c03.-->
<xsl:template name="hidebox-c03-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c04.-->
<xsl:template name="showbox-c04-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/><td/><td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c04.-->
<xsl:template name="hidebox-c04-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c05.-->
<xsl:template name="showbox-c05-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c05.-->
<xsl:template name="hidebox-c05-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c06.-->
<xsl:template name="showbox-c06-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c06.-->
<xsl:template name="hidebox-c06-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c07.-->
<xsl:template name="showbox-c07-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c07.-->
<xsl:template name="hidebox-c07-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c08.-->
<xsl:template name="showbox-c08-box-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td/><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c08.-->
<xsl:template name="hidebox-c08-box-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<xsl:template name="components-boxplusfolder">

<!-- Process each c01.-->
<xsl:for-each select="c01">

<table width="100%">
<tr><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
</tr>
<a><xsl:attribute name="name">series<xsl:number/>
</xsl:attribute>
</a>

<!-- Processes each c01/did.  Box and folder numbers are never associated with c01 in this stylesheet so no testing for their appearance is necessary.-->

<xsl:for-each select="did">
<tr>
<td colspan="14"><h3>
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
<xsl:text> </xsl:text>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</h3>
</td>
</tr>


<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>

                                  
<!-- Proceses each c02.-->
<xsl:for-each select="c02">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>


<xsl:choose>
<xsl:when test="not(../preceding-sibling::c02/did[container[@type='box']=$box-number]    or ../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c02-boxplusfolder"/>
</xsl:when>

<!-- If it did appear before, hide it here.-->
<xsl:otherwise>
<xsl:call-template name="hidebox-c02-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c03.-->
<xsl:for-each select="c03">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c03//did[container[@type='box']=$box-number]     or ../parent::c02/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c03-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c03-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c03 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c04.-->
<xsl:for-each select="c04">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../parent::c03/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c03//did[container[@type='box']=$box-number]     or ../../parent::c02/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">


<xsl:call-template name="showbox-c04-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c04-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c04 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c05-->
<xsl:for-each select="c05">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->

<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c05/did[container[@type='box']=$box-number]     or ../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../parent::c04/did[container[@type='box']=$box-number]    or ../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../parent::c03/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

<xsl:call-template name="showbox-c05-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c05-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c05 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c06-->
<xsl:for-each select="c06">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c06//did[container[@type='box']=$box-number]     or ../parent::c05/did[container[@type='box']=$box-number]      or ../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../parent::c04/did[container[@type='box']=$box-number]    or ../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">



<xsl:call-template name="showbox-c06-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c06-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c06 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c07.-->
<xsl:for-each select="c07">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c07//did[container[@type='box']=$box-number]     or ../parent::c06/did[container[@type='box']=$box-number]     or ../../preceding-sibling::c06//did[container[@type='box']=$box-number]      or ../../parent::c05/did[container[@type='box']=$box-number]      or ../../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../../parent::c04/did[container[@type='box']=$box-number]    or ../../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">


<xsl:call-template name="showbox-c07-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c07-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c07 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c08.-->
<xsl:for-each select="c08">
<xsl:for-each select="did">

<!-- Creates a variable called box-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-number" select="container[@type='box']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c08//did[container[@type='box']=$box-number]     or ../parent::c07/did[container[@type='box']=$box-number]     or ../../preceding-sibling::c07//did[container[@type='box']=$box-number]     or ../../parent::c06/did[container[@type='box']=$box-number]     or ../../../preceding-sibling::c06//did[container[@type='box']=$box-number]      or ../../../parent::c05/did[container[@type='box']=$box-number]      or ../../../../preceding-sibling::c05//did[container[@type='box']=$box-number]     or ../../../../parent::c04/did[container[@type='box']=$box-number]    or ../../../../../preceding-sibling::c04//did[container[@type='box']=$box-number]     or ../../../../../parent::c03/did[container[@type='box']=$box-number]     or ../../../../../../preceding-sibling::c03//did[container[@type='box']=$box-number]    or ../../../../../../parent::c02/did[container[@type='box']=$box-number]     or ../../../../../../../preceding-sibling::c02//did[container[@type='box']=$box-number])">

   

<xsl:call-template name="showbox-c08-boxplusfolder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c08-boxplusfolder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c08 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</table>
<hr/>
<br/>
<br/>

</xsl:for-each>
</xsl:template>


<!-- Shows the box and folder numbers for a c02.-->
<xsl:template name="showbox-c02-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>
<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
 </td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
 </td>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c02.-->
<xsl:template name="hidebox-c02-boxplusfolder">
<tr>
<td valign="top"/>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c03.-->
<xsl:template name="showbox-c03-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/>
<td colspan="9" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td></tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c03.-->
<xsl:template name="hidebox-c03-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td>
<td colspan="9" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c04.-->
<xsl:template name="showbox-c04-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td colspan="8" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c04.-->
<xsl:template name="hidebox-c04-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td>
<td/>
<td colspan="8" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c05.-->
<xsl:template name="showbox-c05-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/>
<td colspan="7" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c05.-->
<xsl:template name="hidebox-c05-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td><td/><td/>
<td colspan="7" valign="top">

<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c06.-->
<xsl:template name="showbox-c06-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c06.-->
<xsl:template name="hidebox-c06-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c07.-->
<xsl:template name="showbox-c07-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c07.-->
<xsl:template name="hidebox-c07-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>


<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box and folder number for a c08.-->
<xsl:template name="showbox-c08-boxplusfolder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box</xsl:text></font></b>
</td>
<td><b><font size="-1">
<xsl:text>Folder</xsl:text>
</font></b></td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box']"/>
</td>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text><xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box and folder number for a c08.-->
<xsl:template name="hidebox-c08-boxplusfolder">
<tr>
<td valign="top"> </td>
<td valign="top">
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td valign="top"> </td><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

 

<!-- A named template that formats components that contain only folder numbers.-->

<xsl:template name="components-folder-only">

<!-- Process each c01.-->
<xsl:for-each select="c01">

<table width="100%">
<tr><td width="12%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="4%"> </td>
</tr>
<a><xsl:attribute name="name">series<xsl:number/>
</xsl:attribute>
</a>

<!-- Processes each c01/did.  Folder numbers are never associated with c01 in this stylesheet so no testing for their appearance is necessary.-->

<xsl:for-each select="did">
<tr>
<td colspan="14"><h3>
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</h3>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>


                                  
<!-- Proceses each c02.-->
<xsl:for-each select="c02">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c02/did[container[@type='folder']=$folder-number]    or ../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">

<xsl:call-template name="showfolder-c02-folder-only"/>
</xsl:when>

<!-- If it did appear before, hide it here.-->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c02-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td colspan="12" valign="top">
<xsl:apply-templates select="."/>

</td></tr>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c03.-->
<xsl:for-each select="c03">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c03//did[container[@type='folder']=$folder-number]     or ../parent::c02/did[container[@type='folder']=$folder-number]    or ../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">

<xsl:call-template name="showfolder-c03-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c03-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c03 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td colspan="11" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c04.-->
<xsl:for-each select="c04">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c04//did[container[@type='folder']=$folder-number]     or ../parent::c03/did[container[@type='folder']=$folder-number]    or ../../preceding-sibling::c03//did[container[@type='folder']=$folder-number]     or ../../parent::c02/did[container[@type='folder']=$folder-number]     or ../../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">


<xsl:call-template name="showfolder-c04-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c04-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c04 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c05-->
<xsl:for-each select="c05">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c05/did[container[@type='folder']=$folder-number]     or ../preceding-sibling::c05//did[container[@type='folder']=$folder-number]     or ../parent::c04/did[container[@type='folder']=$folder-number]    or ../../preceding-sibling::c04//did[container[@type='folder']=$folder-number]     or ../../parent::c03/did[container[@type='folder']=$folder-number]     or ../../../preceding-sibling::c03//did[container[@type='folder']=$folder-number]    or ../../../parent::c02/did[container[@type='folder']=$folder-number]     or ../../../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">

<xsl:call-template name="showfolder-c05-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c05-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c05 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c06-->
<xsl:for-each select="c06">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>


<xsl:choose>

<xsl:when test="not(../preceding-sibling::c06//did[container[@type='folder']=$folder-number]     or ../parent::c05/did[container[@type='folder']=$folder-number]      or ../../preceding-sibling::c05//did[container[@type='folder']=$folder-number]     or ../../parent::c04/did[container[@type='folder']=$folder-number]    or ../../../preceding-sibling::c04//did[container[@type='folder']=$folder-number]     or ../../../parent::c03/did[container[@type='folder']=$folder-number]     or ../../../../preceding-sibling::c03//did[container[@type='folder']=$folder-number]    or ../../../../parent::c02/did[container[@type='folder']=$folder-number]     or ../../../../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">



<xsl:call-template name="showfolder-c06-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c06-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c06 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c07.-->
<xsl:for-each select="c07">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c07//did[container[@type='folder']=$folder-number]     or ../parent::c06/did[container[@type='folder']=$folder-number]     or ../../preceding-sibling::c06//did[container[@type='folder']=$folder-number]      or ../../parent::c05/did[container[@type='folder']=$folder-number]      or ../../../preceding-sibling::c05//did[container[@type='folder']=$folder-number]     or ../../../parent::c04/did[container[@type='folder']=$folder-number]    or ../../../../preceding-sibling::c04//did[container[@type='folder']=$folder-number]     or ../../../../parent::c03/did[container[@type='folder']=$folder-number]     or ../../../../../preceding-sibling::c03//did[container[@type='folder']=$folder-number]    or ../../../../../parent::c02/did[container[@type='folder']=$folder-number]     or ../../../../../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">

   

<xsl:call-template name="showfolder-c07-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c07-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c07 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c08.-->
<xsl:for-each select="c08">
<xsl:for-each select="did">

<!-- Creates a variable called folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="folder-number" select="container[@type='folder']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c08//did[container[@type='folder']=$folder-number]     or ../parent::c07/did[container[@type='folder']=$folder-number]     or ../../preceding-sibling::c07//did[container[@type='folder']=$folder-number]     or ../../parent::c06/did[container[@type='folder']=$folder-number]     or ../../../preceding-sibling::c06//did[container[@type='folder']=$folder-number]      or ../../../parent::c05/did[container[@type='folder']=$folder-number]      or ../../../../preceding-sibling::c05//did[container[@type='folder']=$folder-number]     or ../../../../parent::c04/did[container[@type='folder']=$folder-number]    or ../../../../../preceding-sibling::c04//did[container[@type='folder']=$folder-number]     or ../../../../../parent::c03/did[container[@type='folder']=$folder-number]     or ../../../../../../preceding-sibling::c03//did[container[@type='folder']=$folder-number]    or ../../../../../../parent::c02/did[container[@type='folder']=$folder-number]     or ../../../../../../../preceding-sibling::c02//did[container[@type='folder']=$folder-number])">

   

<xsl:call-template name="showfolder-c08-folder-only"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidefolder-c08-folder-only"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c08 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</table>
<hr/>
<br/>
<br/>

</xsl:for-each>
</xsl:template>


<!-- Shows the box and folder numbers for a c02.-->
<xsl:template name="showfolder-c02-folder-only">
<tr>
<td>
<b><font size="-1">
<xsl:text>Folder</xsl:text></font></b>
</td>
</tr>
<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="abstract"/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c02.-->
<xsl:template name="hidefolder-c02-folder-only">
<tr>
<td valign="top"/>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c03. Column head is not shown.-->
<xsl:template name="showfolder-c03-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c03.-->
<xsl:template name="hidefolder-c03-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c04. Column head is not shown.-->
<xsl:template name="showfolder-c04-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>
<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c04.-->
<xsl:template name="hidefolder-c04-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c05. Column head not shown.-->
<xsl:template name="showfolder-c05-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c05.-->
<xsl:template name="hidefolder-c05-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c06.  Column head is not given.-->
<xsl:template name="showfolder-c06-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c06.-->
<xsl:template name="hidefolder-c06-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c07. The column head is not included.-->
<xsl:template name="showfolder-c07-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c07.-->
<xsl:template name="hidefolder-c07-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box number for a c08. The column head is not displayed-->
<xsl:template name="showfolder-c08-folder-only">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='folder']"/>
</td>
<td/><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box number for a c08.-->
<xsl:template name="hidefolder-c08-folder-only">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>



<!-- A named template that formats did elements for components that contain combined box-folder numbers.-->

<xsl:template name="components-box-folder">

<!-- Process each c01.-->
<xsl:for-each select="c01">

<table width="100%">
<tr><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
</tr>
<a><xsl:attribute name="name">series<xsl:number/>
</xsl:attribute>
</a>

<!-- Processes each c01/did.  Box-folder numbers are never associated with c01 in this stylesheet so no testing for their appearance is necessary.-->

<xsl:for-each select="did">
<tr>
<td colspan="14"><h3>
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</h3>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>


                                  
<!-- Proceses each c02.-->
<xsl:for-each select="c02">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c02/did[container[@type='box-folder']=$box-folder-number]    or ../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">

<xsl:call-template name="showbox-c02-box-folder"/>
</xsl:when>

<!-- If it did appear before, hide it here.-->
<xsl:otherwise>
<xsl:call-template name="hidebox-c02-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td colspan="12" valign="top">
<xsl:apply-templates select="."/>

</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c03.-->
<xsl:for-each select="c03">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c02/did[container[@type='box-folder']=$box-folder-number]    or ../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">

<xsl:call-template name="showbox-c03-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c03-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c03 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td colspan="11" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c04.-->
<xsl:for-each select="c04">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c04//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c03/did[container[@type='box-folder']=$box-folder-number]    or ../../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]     or ../../parent::c02/did[container[@type='box-folder']=$box-folder-number]     or ../../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">


<xsl:call-template name="showbox-c04-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c04-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c04 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c05-->
<xsl:for-each select="c05">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>
<xsl:when test="not(../preceding-sibling::c05/did[container[@type='box-folder']=$box-folder-number]     or ../preceding-sibling::c05//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c04/did[container[@type='box-folder']=$box-folder-number]    or ../../preceding-sibling::c04//did[container[@type='box-folder']=$box-folder-number]     or ../../parent::c03/did[container[@type='box-folder']=$box-folder-number]     or ../../../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]    or ../../../parent::c02/did[container[@type='box-folder']=$box-folder-number]     or ../../../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">

<xsl:call-template name="showbox-c05-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c05-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c05 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c06-->
<xsl:for-each select="c06">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>


<xsl:choose>

<xsl:when test="not(../preceding-sibling::c06//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c05/did[container[@type='box-folder']=$box-folder-number]      or ../../preceding-sibling::c05//did[container[@type='box-folder']=$box-folder-number]     or ../../parent::c04/did[container[@type='box-folder']=$box-folder-number]    or ../../../preceding-sibling::c04//did[container[@type='box-folder']=$box-folder-number]     or ../../../parent::c03/did[container[@type='box-folder']=$box-folder-number]     or ../../../../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]    or ../../../../parent::c02/did[container[@type='box-folder']=$box-folder-number]     or ../../../../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">



<xsl:call-template name="showbox-c06-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c06-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c06 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c07.-->
<xsl:for-each select="c07">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c07//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c06/did[container[@type='box-folder']=$box-folder-number]     or ../../preceding-sibling::c06//did[container[@type='box-folder']=$box-folder-number]      or ../../parent::c05/did[container[@type='box-folder']=$box-folder-number]      or ../../../preceding-sibling::c05//did[container[@type='box-folder']=$box-folder-number]     or ../../../parent::c04/did[container[@type='box-folder']=$box-folder-number]    or ../../../../preceding-sibling::c04//did[container[@type='box-folder']=$box-folder-number]     or ../../../../parent::c03/did[container[@type='box-folder']=$box-folder-number]     or ../../../../../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]    or ../../../../../parent::c02/did[container[@type='box-folder']=$box-folder-number]     or ../../../../../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">

   

<xsl:call-template name="showbox-c07-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c07-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c07 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c08.-->
<xsl:for-each select="c08">
<xsl:for-each select="did">

<!-- Creates a variable called box-folder-number whose value is equal to the value of the container element for the current component.-->
<xsl:variable name="box-folder-number" select="container[@type='box-folder']"/>

<xsl:choose>

<xsl:when test="not(../preceding-sibling::c08//did[container[@type='box-folder']=$box-folder-number]     or ../parent::c07/did[container[@type='box-folder']=$box-folder-number]     or ../../preceding-sibling::c07//did[container[@type='box-folder']=$box-folder-number]     or ../../parent::c06/did[container[@type='box-folder']=$box-folder-number]     or ../../../preceding-sibling::c06//did[container[@type='box-folder']=$box-folder-number]      or ../../../parent::c05/did[container[@type='box-folder']=$box-folder-number]      or ../../../../preceding-sibling::c05//did[container[@type='box-folder']=$box-folder-number]     or ../../../../parent::c04/did[container[@type='box-folder']=$box-folder-number]    or ../../../../../preceding-sibling::c04//did[container[@type='box-folder']=$box-folder-number]     or ../../../../../parent::c03/did[container[@type='box-folder']=$box-folder-number]     or ../../../../../../preceding-sibling::c03//did[container[@type='box-folder']=$box-folder-number]    or ../../../../../../parent::c02/did[container[@type='box-folder']=$box-folder-number]     or ../../../../../../../preceding-sibling::c02//did[container[@type='box-folder']=$box-folder-number])">

   

<xsl:call-template name="showbox-c08-box-folder"/>
</xsl:when>


<!-- If the box number did appear in a previous component, hide it here. -->
<xsl:otherwise>
<xsl:call-template name="hidebox-c08-box-folder"/>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- Process any remaining c08 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</table>
<hr/>
<br/>
<br/>

</xsl:for-each>
</xsl:template>

<!-- Shows the box/folder numbers for a c02.  The column head, box/folder, appears.-->
<xsl:template name="showbox-c02-box-folder">
<tr>
<td>
<b><font size="-1">
<xsl:text>Box/folder</xsl:text></font></b>
</td>
</tr>

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c02.-->
<xsl:template name="hidebox-c02-box-folder">
<tr>
<td valign="top"/>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates select="."/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c03.  The column head, box/folder, does not appear.-->
<xsl:template name="showbox-c03-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c03.-->
<xsl:template name="hidebox-c03-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c04.  The column head, box/folder, does not appear.-->
<xsl:template name="showbox-c04-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/><td/><td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c04.-->
<xsl:template name="hidebox-c04-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c05. The column head, box/folder, does not appear. -->
<xsl:template name="showbox-c05-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c05.-->
<xsl:template name="hidebox-c05-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c06.  The column head, box/folder, does not appear.-->
<xsl:template name="showbox-c06-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c06.-->
<xsl:template name="hidebox-c06-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/>
<td colspan="6" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c07.  The column heading, box/folder, does not appear.-->
<xsl:template name="showbox-c07-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c07.-->
<xsl:template name="hidebox-c07-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/>
<td colspan="5" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- Shows the box/folder number for a c08.  The columnheading, box/folder, does not appear.-->
<xsl:template name="showbox-c08-box-folder">

<tr>
<td valign="top"> 
<xsl:apply-templates select="container[@type='box-folder']"/>
</td>
<td/><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--Hides the box/folder number for a c08.-->
<xsl:template name="hidebox-c08-box-folder">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/><td/>
<td colspan="4" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!-- A named template that formats did elements for components that have no container elements  given.-->

<xsl:template name="no-container">

<!-- Process each c01.-->
<xsl:for-each select="c01">

<table width="100%">
<tr><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
<td width="7%"> </td><td width="7%"> </td><td width="7%"> </td>
</tr>
<a><xsl:attribute name="name">series<xsl:number/>
</xsl:attribute>
</a>

<!-- Processes each c01/did. -->

<xsl:for-each select="did">
<tr>
<td colspan="14"><h3>
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</h3>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/>
<td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td colspan="13" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>


                                  
<!-- Proceses each c02.-->
<xsl:for-each select="c02">
<xsl:for-each select="did">

<tr>
<td valign="top"/>
<td colspan="13">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>

<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td colspan="12" valign="top">
<xsl:apply-templates select="."/>

</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c03.-->

<xsl:for-each select="c03">
<xsl:for-each select="did">
<tr colspan="12">
<td valign="top"> </td>
<td valign="top"> </td>
<td valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>

<!-- Process any remaining c03 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td colspan="11" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c04.-->
<xsl:for-each select="c04">
<xsl:for-each select="did">
<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td colspan="11" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>

<!-- Process any remaining c04 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td colspan="10" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c05-->
<xsl:for-each select="c05">
<xsl:for-each select="did">

<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td/>
<td colspan="10" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td>
</tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>

<!-- Process any remaining c05 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td colspan="9" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c06-->
<xsl:for-each select="c06">
<xsl:for-each select="did">

<tr>
<td valign="top"> </td>
<td valign="top"> </td>
<td/>
<td/>
<td/>
<td colspan="9" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td colspan="5" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>

<!-- Process any remaining c06 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td colspan="8" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c07.-->
<xsl:for-each select="c07">
<xsl:for-each select="did">
<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/>
<td colspan="8" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td colspan="4" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>

</xsl:for-each>

<!-- Process any remaining c07 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td colspan="7" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

<!-- Processes each c08.-->
<xsl:for-each select="c08">
<xsl:for-each select="did">

<tr>
<td valign="top"> </td>
<td valign="top"> </td><td/><td/><td/><td/><td/>
<td colspan="7" valign="top">
<xsl:if test="unitid">
<xsl:for-each select="unitid">
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:choose>
<xsl:when test="unittitle/unitdate">
<xsl:for-each select="unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="unitdate"/>
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="physdesc">
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text> </xsl:text>
</xsl:for-each>
</td>
</tr>

<xsl:if test="abstract[string-length(text()|*)!=0] | note[string-length(text()|*)!=0]">
<xsl:for-each select="abstract | note">
<tr>
<td/><td/><td/><td/><td/><td/><td/><td/><td colspan="3" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:if>
</xsl:for-each>

<!-- Process any remaining c08 elements of the type specified.-->
<xsl:for-each select="scopecontent | bioghist | note | odd |admininfo/accessrestrict | admininfo/userestrict">
<xsl:for-each select="p">
<tr><td/><td/><td/><td/><td/><td/><td/><td/><td colspan="6" valign="top">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="*[not(self::p)]">
<xsl:apply-templates/>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</table>
<hr/>
<br/>
<br/>
</xsl:for-each>
</xsl:template>


</xsl:stylesheet>
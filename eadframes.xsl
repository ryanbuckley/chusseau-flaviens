<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:saxon="http://icl.com/saxon" extension-element-prefixes="saxon">

<!-- Stylesheet created 19 October 2004 by Vikram Ahmed, ECU -->

<!-- Reference from stylesheet eadcust.xsl-->
<xsl:include href="eadcust.xsl"/>

<xsl:output omit-xml-declaration="yes"/>

<!-- Parameter Variable declarations-->

<!-- Path of the file-->
<xsl:param name="basepath">file:/c:/notetab/ead/</xsl:param>

<!-- Document name-->
<xsl:param name="docname">
 <xsl:value-of select="//archdesc/did/unitid"/>
</xsl:param>

<!-- Font Size-->
<xsl:param name="basefontsize">13</xsl:param>
<xsl:param name="basefontsize1">11</xsl:param>

<!-- Path of the frame html-->
<xsl:variable name="framepath">
	<xsl:value-of select="$basepath"/>
	<xsl:value-of select="$docname"/>
	<xsl:text>.frame.html</xsl:text>
</xsl:variable>

<!-- Path of the toc html-->
<xsl:variable name="tocpath">
	<xsl:value-of select="$basepath"/>
	<xsl:value-of select="$docname"/>
	<xsl:text>.toc.html</xsl:text>
</xsl:variable>

<!-- Path of the body html-->
<xsl:variable name="bodypath">
	<xsl:value-of select="$basepath"/>
	<xsl:value-of select="$docname"/>
	<xsl:text>.body.html</xsl:text>
</xsl:variable>

<!-- Match Root Element -->
<xsl:template match="/">

<!-- Create frame html -->
<xsl:document method="html" indent="yes" href="{$framepath}" encoding="iso-8859-1">
	<xsl:element name="html">
		<xsl:element name="head">

   <!-- Dublin Core Elements in frame html-->
    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Title</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#title</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Creator</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/origination"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#creator</xsl:text>
	</xsl:attribute>
    </xsl:element>

   <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[1]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[2]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/persname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/corpname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/subject"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/geogname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/famname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/genreform"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/title"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Publisher</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/repository"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#publisher</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Date</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle/unitdate/@normal"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#date</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Identifier</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//eadheader/eadid/@publicid"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#identifier</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Language</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/langmaterial/language/@langcode"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#language</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Rights</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/descgrp/userestrict"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#rights</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Medium</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/genreform"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[1]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[1]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[2]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[2]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<!-- Create frame page -->
			<xsl:element name="title">
				<xsl:value-of select="//titlepage/titleproper"/>
			</xsl:element>
			<xsl:element name="frameset">
				<xsl:attribute name="border">0</xsl:attribute>
				<xsl:attribute name="cols">220,*</xsl:attribute>
				<xsl:element name="frame">
					<xsl:attribute name="name">
						<xsl:text>toc</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="src">
						<xsl:value-of select="$docname"/>
						<xsl:text>.toc.html</xsl:text>
					</xsl:attribute>
				</xsl:element>
				<xsl:element name="frame">
					<xsl:attribute name="name">
						<xsl:text>body</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="src">
						<xsl:value-of select="$docname"/>
						<xsl:text>.body.html</xsl:text>	
					</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:document>
<!-- End frame html -->

<!-- Create Table of contents (toc) html -->
<xsl:document method="html" indent="yes" href="{$tocpath}" encoding="iso-8859-1">
	<xsl:element name="html">
		<xsl:element name="head">

   <!-- Dublin Core Elements in toc html-->
    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Title</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#title</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Creator</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/origination"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#creator</xsl:text>
	</xsl:attribute>
    </xsl:element>

   <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[1]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[2]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/persname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/corpname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/subject"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/geogname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/famname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/genreform"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/title"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Publisher</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/repository"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#publisher</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Date</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle/unitdate/@normal"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#date</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Identifier</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//eadheader/eadid/@publicid"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#identifier</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Language</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/langmaterial/language/@langcode"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#language</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Rights</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/descgrp/userestrict"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#rights</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Medium</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/genreform"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[1]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[1]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[2]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[2]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

	<!-- Setting background color and font color in toc -->
			<xsl:element name="style">
				<xsl:text>h1, h2, h3, h4, h5, h6</xsl:text>
				<xsl:text>{font-family: "Times New Roman", Arial, Helvetica, sans-serif; color:</xsl:text>
					<xsl:value-of select="$headcolor"/>
				<xsl:text>}</xsl:text>
			</xsl:element>
		</xsl:element>
		<xsl:element name="body">
			<xsl:attribute name="style">
				<xsl:text>margin-top:0.05in;margin-left:0.15in;margin-right:0.15in;margin-bottom:0.25in;</xsl:text>
				<xsl:text>background-color:</xsl:text>
				<xsl:value-of select="$headcolor"/>
			</xsl:attribute>

			<!-- Apply Table of Contents Template  -->
			<xsl:apply-templates select="ead/archdesc" mode="toc"/>

			<!-- Create NCEAD statement and link-->			

			<xsl:element name="br"/>
			<xsl:element name="br"/>
			<xsl:element name="br"/>
			<xsl:element name="br"/>
			 <xsl:element name="center">
			  <xsl:element name="a">
			    <xsl:attribute name="target">
			      <xsl:text>_top</xsl:text>
			    </xsl:attribute>    	
			    <xsl:attribute name="href">http://www.ncecho.org/ncead</xsl:attribute>
			     <xsl:element name="img">
			      <xsl:attribute name="src">
			       <xsl:text>seals/nceadseal.jpg</xsl:text>
			      </xsl:attribute>
			      <xsl:attribute name="border">0</xsl:attribute>
			     </xsl:element>
			  </xsl:element>
			<xsl:for-each select="//filedesc/notestmt/note/p">
			 <xsl:element name="div">
				<xsl:attribute name="style">
				<xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; FONT-WEIGHT:bold; FONT-SIZE:13px; color:</xsl:text>
					<xsl:value-of select="$pcolor"/>
				</xsl:attribute>
				<xsl:apply-templates/>
			</xsl:element>
			</xsl:for-each>
		       </xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:document>
<!-- End toc html -->

<!-- Create body html -->
<xsl:document method="html" indent="yes" href="{$bodypath}" encoding="iso-8859-1">
	<xsl:element name="html">
		<xsl:element name="head">

<!-- Dublin Core Elements in body html-->
    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Title</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#title</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Creator</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/origination"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#creator</xsl:text>
	</xsl:attribute>
    </xsl:element>

   <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[1]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

    <xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Description</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/abstract[2]"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#description</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/persname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/corpname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/subject"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/geogname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/famname"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/genreform"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/controlaccess/list/item/title"> 
<xsl:element name="META">
<xsl:attribute name="name">DC.Subject</xsl:attribute>
<xsl:attribute name="CONTENT">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
<xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#subject</xsl:text>
	</xsl:attribute>
    </xsl:element>
</xsl:for-each>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Publisher</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/repository"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#publisher</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Date</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/unittitle/unitdate/@normal"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#date</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Identifier</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//eadheader/eadid/@publicid"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#identifier</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Language</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/langmaterial/language/@langcode"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#language</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Rights</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/descgrp/userestrict"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#rights</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Medium</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/genreform"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[1]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[1]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

<xsl:element name="META">
      <xsl:attribute name="NAME">
        <xsl:text>DC.Format.Extent</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="CONTENT">
       	<xsl:value-of select="//archdesc/did/physdesc/extent[2]"/><xsl:text> </xsl:text><xsl:value-of select="//archdesc/did/physdesc/extent[2]/@unit"/>
      </xsl:attribute>	
    </xsl:element>
    <xsl:element name="LINK">
	<xsl:attribute name="REL">
    		<xsl:text>SCHEMA.dc</xsl:text>
 	</xsl:attribute>
	<xsl:attribute name="HREF">
		<xsl:text>http://purl.org/metadata/dublin_core_elements#format</xsl:text>
	</xsl:attribute>
    </xsl:element>

	<!-- Background and Font Settings -->

			<xsl:element name="style">
				<xsl:text>h1, h2, h3, h4, h5, h6</xsl:text>
				<xsl:text>{font-family: "Times New Roman", Arial, Helvetica, sans-serif; color:</xsl:text>
					<xsl:value-of select="$headcolor"/>
				<xsl:text>}</xsl:text>
			</xsl:element>
		</xsl:element>
		<xsl:element name="body">
			<xsl:attribute name="style">
				<xsl:text>background-color:</xsl:text><xsl:value-of select="$pcolor"/>
				<xsl:text>; margin-top:0.25in;margin-left:0.50in;margin-right:0.50in;margin-bottom:3.0in;</xsl:text>
				<xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="ead/frontmatter"/>
			<xsl:apply-templates select="ead/archdesc"/>
		</xsl:element>
	</xsl:element>
</xsl:document>
</xsl:template>

<!-- Matching Templates (some templates called from eadshared.xsl) -->

<xsl:template match="frontmatter">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="//titlepage/subtitle">
<xsl:element name="h2">
		<xsl:attribute name="style">
			<xsl:text>text-decoration:none;color:</xsl:text>
			<xsl:value-of select="$headcolor"/>
			</xsl:attribute>
			<xsl:value-of select="//titlepage/subtitle"/>
	  	</xsl:element> 
</xsl:template>

<xsl:template match="titlepage">
 	<xsl:element name="a">
				<xsl:call-template name="addidtoc"/><!-- from eadshared.xsl -->
         <xsl:apply-templates select="extref"><!-- from eadshared.xsl -->
				 </xsl:apply-templates>	
							<xsl:attribute name="target">
								<xsl:text>_top</xsl:text>
							</xsl:attribute>    	
	<xsl:apply-templates/>
	</xsl:element>

</xsl:template>

<xsl:template match="titlepage/titleproper">
	<xsl:element name="h1">
		<xsl:call-template name="addid"/><!-- from eadshared.xsl -->
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<xsl:template match="titlepage/author">
	<xsl:element name="h2">
		<xsl:call-template name="addid"/><!-- from eadshared.xsl -->
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<xsl:template match="titlepage/publisher">
    <xsl:element name="a">
<xsl:attribute name="target">
				<xsl:text>_top</xsl:text>
		</xsl:attribute>    	
<xsl:attribute name="href"><xsl:value-of select="$website"/></xsl:attribute>
         <xsl:apply-templates/>
    </xsl:element> <!-- end: a -->
</xsl:template>

<xsl:template match="titlepage/date">

</xsl:template>


<xsl:template match="titlepage/num |                       titlepage/bibseries">
	<xsl:element name="h3">
		<xsl:call-template name="addid"/><!-- from eadshared.xsl -->
		<xsl:apply-templates/>
	</xsl:element>
	<xsl:element name="br"/>
</xsl:template>

<xsl:template match="titlepage/defitem/item/extptr">
    <xsl:element name="a">

<xsl:attribute name="href">http://www.ncecho.org</xsl:attribute>
         <xsl:apply-templates select="titlepage/defitem/item/extptr"/>
<xsl:attribute name="target">
				<xsl:text>_top</xsl:text>
		</xsl:attribute>    	
    </xsl:element> <!-- end: a -->
</xsl:template>

<xsl:template match="titlepage/sponsor">
	<xsl:element name="br"/>
	<xsl:element name="p">
			<xsl:apply-templates/>
		</xsl:element>
</xsl:template>


<!-- Blank template as this works only in Non-Frames version -->
<xsl:template name="back">
</xsl:template>

<!-- Reference from stylesheet eadshared.xsl-->
<xsl:include href="eadshared.xsl"/>

<!-- table of contents entries-->

<!-- toc settings for upper level-->
<xsl:template name="tocentry">
	<xsl:param name="headlevel">h3</xsl:param>
	<xsl:param name="headmargin">8</xsl:param> 
		<xsl:element name="a">
			<xsl:attribute name="style">
				<xsl:text>text-decoration:none;color:</xsl:text>
				<xsl:value-of select="$pcolor"/>
			</xsl:attribute>
			<xsl:attribute name="target">
				
				<xsl:text>body</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="href">
				<xsl:value-of select="$docname"/>
				<xsl:text>.body.html#</xsl:text>
				<xsl:choose>
					<xsl:when test="@id">
						<xsl:value-of select="@id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="generate-id()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

<!-- Font sizes of upper levels in table of contents-->
  			<xsl:element name="div"> 
       		<xsl:attribute name="style">
					<xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; FONT-WEIGHT:bold; FONT-SIZE:</xsl:text>
					<xsl:value-of select="$basefontsize"/>
<xsl:text>px; MARGIN-LEFT: </xsl:text>
					<xsl:value-of select="$headmargin"/>
				<xsl:text>px; COLOR: </xsl:text><xsl:value-of select="$pcolor"/><xsl:text>;</xsl:text>
				</xsl:attribute>
				<xsl:value-of select="head | did/unittitle"/>
			</xsl:element>
		</xsl:element>
</xsl:template>

<!-- toc settings for lower level(sub-level)-->

<xsl:template name="tocentry1">
	<xsl:param name="headlevel">h3</xsl:param>
	<xsl:param name="headmargin">8</xsl:param>
		<xsl:element name="a">
			<xsl:attribute name="style">
				<xsl:text>text-decoration:none;color:</xsl:text>
				<xsl:value-of select="$pcolor"/>
			</xsl:attribute>
			<xsl:attribute name="target">
				
				<xsl:text>body</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="href">
				<!-- <xsl:value-of select="$basepath"/> -->
				<xsl:value-of select="$docname"/>
				<xsl:text>.body.html#</xsl:text>
				<xsl:choose>
					<xsl:when test="@id">
						<xsl:value-of select="@id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="generate-id()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

<!-- Font sizes of lower levels (sub-levels) in table of contents-->
  			<xsl:element name="div"> 
       		<xsl:attribute name="style">
					<xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; FONT-WEIGHT:bold; FONT-SIZE:</xsl:text>
					<xsl:value-of select="$basefontsize1"/>
<xsl:text>px; MARGIN-LEFT: </xsl:text>
					<xsl:value-of select="$headmargin"/>
				<xsl:text>px; COLOR: </xsl:text><xsl:value-of select="$pcolor"/><xsl:text>;</xsl:text>
				</xsl:attribute>
				<xsl:value-of select="head | did/unittitle"/>
			</xsl:element>
		</xsl:element>
</xsl:template>


<xsl:template match="archdesc" mode="toc">
		<xsl:element name="div">
<xsl:attribute name="style"><xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; FONT-SIZE:18px; COLOR:</xsl:text><xsl:value-of select="$pcolor"/><xsl:text>; FONT-WEIGHT:bold</xsl:text></xsl:attribute>
Contents of Collection <xsl:value-of select="//archdesc/did/note"/></xsl:element><xsl:element name="div">
<xsl:attribute name="style"><xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; font-weight: bold; FONT-SIZE:8px; COLOR:</xsl:text><xsl:value-of select="$pcolor"/><xsl:text>; </xsl:text></xsl:attribute>
<xsl:element name="br"/>
</xsl:element>

<xsl:element name="a">
			<xsl:attribute name="style">
				<xsl:text>text-decoration:none;color:</xsl:text>
				<xsl:value-of select="$pcolor"/>
			</xsl:attribute>
			<xsl:attribute name="target">
				<xsl:text>body</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="href">
				<xsl:value-of select="$docname"/>
				<xsl:text>.body.html#</xsl:text>
<xsl:for-each select="parent::ead/frontmatter/titlepage">
				<xsl:choose>
					<xsl:when test="@id">
						<xsl:value-of select="@id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="generate-id()"/>
					</xsl:otherwise>
				</xsl:choose>
</xsl:for-each>
			</xsl:attribute>

			<xsl:element name="div">
				<xsl:attribute name="style">
<xsl:text>font-family: "Times New Roman", Arial, Helvetica, sans-serif; FONT-SIZE: 13px; COLOR:</xsl:text> <xsl:value-of select="$pcolor"/><xsl:text>; MARGIN-LEFT: 4px; FONT-WEIGHT: bold</xsl:text>
</xsl:attribute>
				<xsl:text>Titlepage</xsl:text>
			</xsl:element>
		</xsl:element>
	
	<xsl:for-each select="*[head]">
		<xsl:call-template name="tocentry">
			<xsl:with-param name="headlevel">h5</xsl:with-param>
			<xsl:with-param name="basefontsize">16</xsl:with-param>
			<xsl:with-param name="headmargin">4</xsl:with-param>
		</xsl:call-template>
<!-- matches child of did level element with head -->
		<xsl:for-each select="*[head] |              c01[ancestor::dsc/@type='in-depth']                [@level='fonds' or @level='series' or @level='subseries']">
			<xsl:call-template name="tocentry1">
				<xsl:with-param name="headlevel">h6</xsl:with-param>
			<xsl:with-param name="basefontsize1">12</xsl:with-param>
				<xsl:with-param name="headmargin">32</xsl:with-param>
			</xsl:call-template>
<!-- matches child of child of did level element with head -->
			<xsl:for-each select="*[head] |               c02[ancestor::dsc/@type='in-depth']                 [@level='series' or @level='subseries']">
				<xsl:call-template name="tocentry1">
					<xsl:with-param name="headlevel">h6</xsl:with-param>
			<xsl:with-param name="basefontsize1">12</xsl:with-param>
					<xsl:with-param name="headmargin">50</xsl:with-param>
				</xsl:call-template>
<!-- matches child of child of did level element with head -->
			<xsl:for-each select="*[head] |               c03[ancestor::dsc/@type='in-depth']                [@level='series' or @level='subseries']">
				<xsl:call-template name="tocentry1">
					<xsl:with-param name="headlevel">h6</xsl:with-param>
			<xsl:with-param name="basefontsize1">12</xsl:with-param>
					<xsl:with-param name="headmargin">68</xsl:with-param>
				</xsl:call-template>
			</xsl:for-each>

			</xsl:for-each>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>
<!-- end table of contents -->

</xsl:stylesheet>
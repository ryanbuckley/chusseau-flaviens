<!-- EAD2002    Version saxon02a2002  2003 February  -->

<!--  This stylesheet uses Style 3 to generate a frame presentation using the saxon:output extension. Four separate files are output: a basic frameset, two table of contents pages and the body of the fnding aid. -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:saxon="http://icl.com/saxon"
		extension-element-prefixes="saxon" version="1.0">

<xsl:output method="html" doctype-system="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" encoding="utf-8"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="outputdir">C:\eadcb\EADFILES\</xsl:variable>

<xsl:variable name="box-and-folder-heading">
<tr><td width="10%"><b>Box</b></td><td width="10%"><b>Folder</b></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="box-and-volume-heading">
<tr><td width="10%"><b>Box</b></td><td width="10%"><b>Volume</b></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="box-and-item-heading">
<!--Brian edit-->
<tr><td width="10%"><b>Box</b></td><td width="10%"><b>Item</b></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<!--<tr><td width="10%"><b>Box</b></td><td width="10%">&#x00A0;</td><td width="40%"><b>Item</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>-->

<xsl:variable name="box-and-number-heading">
<tr><td width="10%"><b>Box</b></td><td width="10%">&#x00A0;</td><td width="40%"><b>Number</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="volume-and-item-heading">
<tr><td width="10%"><b>Volume</b></td><td width="10%">&#x00A0;</td><td width="40%"><b>Item</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="map-case-and-drawer-heading">
<tr><td width="10%"><b>Map-case</b></td><td width="10%"><b>Drawer</b></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="box-heading">
<tr><td width="10%"><b>Box</b></td><td width="10%">&#x00A0;</td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="volume-heading">
<tr><td width="10%"><b>Volume</b></td><td width="10%"></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="album-heading">
<tr><td width="10%"><b>Album</b></td><td width="10%"></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="reel-heading">
<tr><td width="10%"><b>Reel</b></td><td width="10%"></td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>

<xsl:variable name="folder-heading">
<tr><td width="10%"><b>Folder</b></td><td width="10%">&#x00A0;</td><td width="40%"><b>Title</b></td><td width="40%"><b>Date</b></td></tr>
</xsl:variable>


<xsl:template match="/">

<!-- Creates a variable equal to the value of the number in eadid which serves as the base
 for file names for the various components of the frameset. -->
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>

<!-- The following four saxon:output statements create the four output files that comprise the 
frameset. -->

<saxon:output method="html" href="{$outputdir}{$file}f.html">
   <xsl:call-template name="frameset"/>
</saxon:output>

<!-- Creates the basic table of contents. --> 
<saxon:output method="html" href="{$outputdir}{$file}t.html">
   <xsl:call-template name="toc"/>
</saxon:output>

<!-- Creates the body of the finding aid. --> 
<saxon:output method="html" href="{$outputdir}{$file}b.html">
   <xsl:call-template name="body"/>
</saxon:output>

<!-- Creates a table of contents with the series titles enumerated. -->
<saxon:output method="html" href="{$outputdir}{$file}p.html">
   <xsl:call-template name="plus"/>
</saxon:output>

</xsl:template>

<!-- Defines the frameset. -->
<xsl:template name="frameset">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<title>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/titleproper"/>
<xsl:text>  </xsl:text>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/subtitle"/>
</title>

<xsl:element name="meta">
<xsl:attribute name="name">dc.title</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/titleproper"/>
<xsl:text>  </xsl:text>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt/subtitle"/>
</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.author</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/archdesc/did/origination"/>
</xsl:attribute>
</xsl:element>

<xsl:for-each select="ead//controlaccess/persname | ead//controlaccess/corpname"> 
<xsl:choose>

<!-- b01 edit - expand '600' -->
<xsl:when test="@encodinganalog='600bb10$a'"> 
<!-- End b01 edit -->

<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<!-- b01 edit - additional '600' -->
<xsl:when test="@encodinganalog='600bb30$a'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>
<!-- End b01 edit -->

<!-- b01 edit - expand '610' -->
<xsl:when test="@encodinganalog='610bb20$a'"> 
<!-- End b01 edit -->

<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:when test="@encodinganalog='611'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<!-- b01 - dc.contributor doesn't seem right for these... -->

<xsl:when test="@encodinganalog='700'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>


<xsl:when test="@encodinganalog='710'"> 
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:when>

<xsl:otherwise>
<xsl:element name="meta">
<xsl:attribute name="name">dc.contributor</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<xsl:for-each select="ead//controlaccess/subject">
<xsl:element name="meta">
<xsl:attribute name="name">dc.subject</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:for-each>

<xsl:element name="meta">
<xsl:attribute name="name">dc.title</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="ead/archdesc/did/unittitle"/>
</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.type</xsl:attribute>
<xsl:attribute name="content">text</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.format</xsl:attribute>
<xsl:attribute name="content">manuscripts</xsl:attribute>
</xsl:element>

<xsl:element name="meta">
<xsl:attribute name="name">dc.format</xsl:attribute>
<xsl:attribute name="content">finding aids</xsl:attribute>
</xsl:element>

<xsl:for-each select="ead//controlaccess/geogname">
<xsl:element name="meta">
<xsl:attribute name="name">dc.coverage</xsl:attribute>
<xsl:attribute name="content">
<xsl:value-of select="."/>
</xsl:attribute>
</xsl:element>
</xsl:for-each>


</head>
<frameset cols="25%,75%">
<frame name="toc" src="{$file}t.html">
</frame>
<frame name="body" src="{$file}b.html">
</frame>
</frameset>
</html>
</xsl:template>

<!-- Defines the base table of contents. -->
<xsl:template name="toc">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<base target="body"></base>
<base font="tahoma"/>
<style>
<!--
a {color:FFFFCC; font-family:trebuchet; font-size:10pt}
-->
</style>
</head>

<body style="font-family:tahoma; font-size:10pt" bgcolor="#660000">

<h4 style="font-face:trebuchet MS; color:FFFFCC">TABLE OF CONTENTS</h4>

<!-- A series of tests determine which elements will be included in the table of contents. -->
<xsl:if test="ead/archdesc/did">
<p><b><a href="{$file}b.html#a1"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/did/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<b>
<a style="color:FFFFCC; font-family:trebuchet; font-size:10pt;" href="{$file}b.html#a2">
<xsl:value-of select="ead/archdesc/bioghist/head"/>
</a>
</b></p>
</xsl:if>

<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a3"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/scopecontent/head"/></font>
</a></b></p>
</xsl:if>


<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0] | ead/archdesc/organization[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a5"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/arrangement/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/userestrict[string-length(text()|*)!=0] | ead/archdesc/accessrestrict[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a14"><font color="FFFFCC">
<xsl:text>Restrictions</xsl:text></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a12"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/controlaccess/head"/></font>
</a></b></p>
</xsl:if>

<!-- b11 edit took out the or clause to separate related from separate material in links -->
<xsl:if test="ead/archdesc/relatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a6"><font color="FFFFCC">
Related Material</font>
</a></b></p>
</xsl:if>

<!-- b11 edit added link for separated material -->
<xsl:if test="ead/archdesc/separatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a7"><font color="FFFFCC">
Separated Material</font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/acqinfo[string-length(text()|*)!=0] | ead/archdesc/processinfo[string-length(text()|*)!=0] |  ead/archdesc/prefercite[string-length(text()|*)!=0] | ead/archdesc/custodialhist[string-length(text()|*)!=0] | ead/archdesc/altformavailable | ead/archdesc/appraisal | ead/archdesc/accruals[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a13"><font color="FFFFCC">
Administrative Information</font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/otherfindaid[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b><a href="{$file}b.html#a8"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/otherfindaid/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a11"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/odd/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bibliography[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a10"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/bibliography/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/index[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a9"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/index/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/dsc[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<a href="{$file}p.html#a45" target="toc">

<!-- b01 edit - chg plus to arrowdn -->
<img src="arrowdn.gif" border="0"></img>
<!-- End b01 edit -->

</a>
<a name="a46">
</a>
<b>
<a href="{$file}b.html#a23">
<xsl:text> </xsl:text><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/dsc/head"/></font>
</a>
</b>
</p>
</xsl:if>

</body>
</html>
</xsl:template>

<!-- Creates a second table of contents with the series titles enumerated. -->
<xsl:template name="plus">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<base target="body"></base>
<style>
<!--
a {color:FFFFCC; font-family:trebuchet; font-size:10pt}
-->
</style>
</head>

<!-- b01 edit - chg E0E0E0 to CCCCCC -->
<!-- b11 edit - chg to 660000 -->
<body style="font-family:tahoma" bgcolor="#660000">
<!-- End b01 edit -->
<!-- End b11 edit -->

<h4 style="font-face:trebuchet MS; color:#FFFFCC">TABLE OF CONTENTS</h4>

<!-- A series of tests determine which elements will be included in the table of contents with series titles enumerated. -->

<xsl:if test="ead/archdesc/did">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a1"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/did/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a2"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/bioghist/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a3"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/scopecontent/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]|ead/archdesc/organization[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a5"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/arrangement/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/userestrict[string-length(text()|*)!=0] | ead/archdesc/accessrestrict[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a14"><font color="FFFFCC">
<xsl:text>Restrictions</xsl:text></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a12"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/controlaccess/head"/></font>
</a></b></p>
</xsl:if>
     
<xsl:if test="ead/archdesc/relatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a6"><font color="FFFFCC">
Related Material</font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/separatedmaterial[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a7"><font color="FFFFCC">
Separated Material</font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/acqinfo[string-length(text()|*)!=0] | ead/archdesc/processinfo[string-length(text()|*)!=0] | ead/archdesc/prefercite[string-length(text()|*)!=0] | ead/archdesc/custodialhist[string-length(text()|*)!=0] | ead/archdesc/altformavailable[string-length(text()|*)!=0] | ead/archdesc/appraisal[string-length(text()|*)!=0] | 
ead/archdesc/accruals[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a13"><font color="FFFFCC">
Administrative Information</font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/otherfindaid[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a8"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/otherfindaid/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a11"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/odd/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/bibliography[string-length(text()|*)!=0]">
<p style="margin-top:-5pt"><b>
<a href="{$file}b.html#a10"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/bibliography/head"/></font>
</a></b></p>
</xsl:if>


<xsl:if test="ead/archdesc/index[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<b><a href="{$file}b.html#a9"><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/index/head"/></font>
</a></b></p>
</xsl:if>

<xsl:if test="ead/archdesc/dsc[string-length(text()|*)!=0]">
<p style="margin-top:-5pt">
<a href="{$file}t.html#a46" target="toc">

<img src="arrowup.gif" border="0"></img>

</a>
<a name="a45"></a>
<b>
<a href="{$file}b.html#a23">
<xsl:text> </xsl:text><font color="FFFFCC">
<xsl:value-of select="ead/archdesc/dsc/head"/></font>
</a></b></p>

<!-- Displays the title and date of each series and numbers
 them to form a hyperlink to the base document. -->
<xsl:for-each select="ead/archdesc/dsc/c01">
<p style="margin-left:10pt; margin-top:-5pt">
<font size="-1">
<a style="color:FFFFCC">
<xsl:attribute name="href">
<xsl:value-of select="$file"/>b.html#series<xsl:number value="position()" format="1"/>
</xsl:attribute>
<xsl:choose>
<xsl:when test="did/unittitle/unitdate">
<xsl:for-each select="did/unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="did/unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="did/unitdate"/>
</xsl:otherwise>
</xsl:choose>
</a>
</font></p>

<xsl:for-each select="child::c02[@level='subseries']">
<p style="margin-left:18pt; margin-top:-5pt">
<font size="-2">
<a style="color:FFFFFF">
<xsl:attribute name="href">
<xsl:value-of select="$file"/>b.html#subseries<xsl:number value="position()" format="1"/>
</xsl:attribute>
<xsl:choose>
<xsl:when test="did/unittitle/unitdate">
<xsl:for-each select="did/unittitle">
<xsl:apply-templates select="text()|*[not(self::unitdate)]"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="./unitdate"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="did/unittitle"/>
<xsl:text> </xsl:text>
<xsl:apply-templates select="did/unitdate"/>
</xsl:otherwise>
</xsl:choose>
</a>
</font></p>
</xsl:for-each>

</xsl:for-each>
</xsl:if>
</body>
</html>
</xsl:template>

<!-- Creates the body of the finding aid. -->
<xsl:template name="body">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>
<html>
<head>
<title>
<xsl:value-of select="ead/eadheader/filedesc/titlestmt"/>
</title>

<style>
h1, h2, h3, h4 {font-family: tahoma} 
</style>
</head>
      <link rel="stylesheet" type="text/css" href="ead.css"/>
<body bgcolor="#FFFFFF">

<xsl:call-template name="eadheader"/>
<xsl:call-template name="archdesc-did"/>
<xsl:call-template name="archdesc-bioghist"/>
<xsl:call-template name="archdesc-scopecontent"/>
<!--<xsl:call-template name="archdesc-organization"/>-->
<xsl:call-template name="archdesc-arrangement"/>
<xsl:call-template name="archdesc-restrict"/>
<xsl:call-template name="archdesc-control"/>
<xsl:call-template name="archdesc-relatedmaterial"/>
<xsl:call-template name="archdesc-separatedmaterial"/>
<xsl:call-template name="archdesc-admininfo"/>
<xsl:call-template name="archdesc-otherfindaid"/>
<xsl:call-template name="archdesc-index"/>
<xsl:call-template name="archdesc-odd"/>
<xsl:call-template name="archdesc-bibliography"/>
<xsl:call-template name="dsc"/>
</body>
</html>
</xsl:template>

<xsl:template match="eadheader/titleproper/date">
<br />
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="num">
<br />
</xsl:template>

<xsl:template name="eadheader">
<xsl:for-each select="ead/eadheader/filedesc/titlestmt">


<center><a name="a0"><img src="ua_logo.gif" border="0"></img></a></center>

<br></br>
<h2><center>

<xsl:apply-templates select="titleproper"/>

</center></h2>
<h3><center>
<xsl:value-of select="subtitle"/>
</center></h3>


<br></br>
</xsl:for-each>

<xsl:for-each select="ead/eadheader/filedesc/publicationstmt">
<br></br>
<h4><xsl:apply-templates select="address"/></h4>
<h5><xsl:value-of select="p"/></h5>
<h5><xsl:value-of select="publisher"/></h5>
<h4><xsl:value-of select="../titlestmt/author"/></h4>
</xsl:for-each>

<xsl:for-each select="ead/eadheader">
<br></br>
<h5><xsl:value-of select="profiledesc"/></h5>
<xsl:if test="revisiondesc">
  <xsl:for-each select="revisiondesc/change">
    <h5><xsl:value-of select="."/></h5>
  </xsl:for-each>
</xsl:if>
<!--<h5><xsl:value-of select="revisiondesc"/></h5>-->
</xsl:for-each>

<hr></hr>
</xsl:template>


<!-- many approaches tried - I believe this is the one that worked -->
<xsl:template match="lb">
<br></br>
</xsl:template>
<!-- End b01 edit -->

<!-- The following templates format the display of various RENDER attributes. -->

<xsl:template match="*/title">
  <xsl:choose>
    <xsl:when test="@render='italic'">
      <i><xsl:apply-templates/></i>
    </xsl:when>
    <xsl:when test="@render='bold'">
      <b><xsl:apply-templates/></b>
    </xsl:when>
    <xsl:when test="@render='bolditalic'">
      <b><i><xsl:apply-templates/></i></b>
    </xsl:when>
    <xsl:when test="@render='super'">
      <sup><xsl:apply-templates/></sup>
    </xsl:when>
    <xsl:when test="@render='sub'">
      <sub><xsl:apply-templates/></sub>
    </xsl:when>
    <xsl:when test="@render='doublequote'">
      &#x201C;<xsl:apply-templates/>&#x201D;
    </xsl:when>
    <xsl:when test="@render='singlequote'">
      &#x2018;<xsl:apply-templates/>&#x2019;
    </xsl:when>
    <xsl:when test="@render='underlined'">
      <u><xsl:apply-templates/></u>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="*/emph">
  <xsl:choose>
    <xsl:when test="@render='italic'">
      <i><xsl:apply-templates/></i>
    </xsl:when>
    <xsl:when test="@render='bold'">
      <b><xsl:apply-templates/></b>
    </xsl:when>
    <xsl:when test="@render='bolditalic'">
      <b><i><xsl:apply-templates/></i></b>
    </xsl:when>
    <xsl:when test="@render='super'">
      <sup><xsl:apply-templates/></sup>
    </xsl:when>
    <xsl:when test="@render='sub'">
      <sub><xsl:apply-templates/></sub>
    </xsl:when>
    <xsl:when test="@render='quoted'">
      &#x201C;<xsl:apply-templates/>&#x201D;
    </xsl:when>
    <xsl:when test="@render='underlined'">
      <u><xsl:apply-templates/></u>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- This template converts a Ref element into an HTML anchor. -->

<xsl:template match="ead/archdesc//ref">
<xsl:variable name="target">
<xsl:value-of select="@target"/>
</xsl:variable>
<a href="#{$target}">
<xsl:value-of select="."/>
</a>
</xsl:template>

<!-- b11 edit added -->
<!-- This template converts a Extref element into an HTML link. -->

<xsl:template match="ead/archdesc//extref">
<xsl:variable name="href">
<xsl:value-of select="@href"/>
</xsl:variable>
<a href="{$href}" target="new">
<xsl:value-of select="."/>
</a>
</xsl:template>

<!-- Converts an ID attribute into the name attribute of an HTML anchor to form the target of a Ref element. -->
  
<xsl:template match="*[@id]">
<a name="{@id}">
<xsl:value-of select="."/>
</a>
</xsl:template>

<!--This template rule formats a list element. -->
<xsl:template match="*/list">
<!-- added 2001/11/13 -->
<xsl:for-each select="head">
<h4 style="margin-left: 30pt">
<xsl:apply-templates/>
</h4>
</xsl:for-each>
<xsl:for-each select="item">
<p style="margin-left: 60pt">
<xsl:apply-templates/>
</p>
</xsl:for-each>
</xsl:template>

<!--Formats a simple table. The width of each column is defined by the colwidth attribute in a colspec element. -->
<xsl:template match="*/table">
<xsl:for-each select="tgroup">
<table width="100%">
<tr>
<xsl:for-each select="colspec">
<td width="{@colwidth}"></td>
</xsl:for-each>
</tr>
<xsl:for-each select="thead">
<xsl:for-each select="row">
<tr>
<xsl:for-each select="entry">
<td valign="top"><b><xsl:value-of select="."/></b>
</td>
</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="tbody">
<xsl:for-each select="row">
<tr>
<xsl:for-each select="entry">
<td valign="top"><xsl:value-of select="."/></td>
</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
</xsl:template>


<!--This template rule formats the top-level did element. -->
<xsl:template name="archdesc-did">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>


<!--For each element of the did, this template inserts the value of the LABEL attribute or, if none is present, a default value. -->

<xsl:for-each select="ead/archdesc/did">
<table width="100%">
<tr><td width="5%"> </td><td width="20%"> </td>
<td width="75"> </td></tr>
<tr><td colspan="3"><h3><a name="a1">
<xsl:apply-templates select="head"/>
</a></h3> </td></tr>


<xsl:if test="origination[string-length(text()|*)!=0]">
<xsl:for-each select="origination">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top"><b>
<xsl:value-of select="@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Creator: </xsl:text></b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>

<!-- Tests for and processes various permutations of unittitle and unitdate. -->
<xsl:for-each select="unittitle">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top"><b>
<xsl:value-of select="@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top"><b>
<xsl:text>Title: </xsl:text>
</b></td><td>
<xsl:apply-templates select="text() |* [not(self::unitdate)]"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>

<xsl:if test="child::unitdate">
<xsl:choose>
<xsl:when test="./unitdate/@label">
<tr><td> </td><td valign="top">
<b>
<xsl:value-of select="./unitdate/@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="./unitdate"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b>
<xsl:text>Dates: </xsl:text>
</b></td><td>
<xsl:apply-templates select="./unitdate"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>
</xsl:for-each>


<!-- Processes the unit date if it is not a child of unit title but a child of did, the current context. -->
<xsl:if test="unitdate">
<xsl:for-each select="unitdate[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="./@label">
<tr><td> </td><td valign="top">
<b>
<xsl:value-of select="./@label"/>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b>
<xsl:text>Dates: </xsl:text>
</b></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>

<xsl:if test="abstract[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="abstract"/>
</td></tr>
</xsl:when>
<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Abstract: </xsl:text></b></td><td>
<xsl:apply-templates select="abstract"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<xsl:if test="physdesc[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="physdesc"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Quantity: </xsl:text></b></td><td>
<xsl:apply-templates select="physdesc"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>


<xsl:if test="unitid[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/><xsl:text>: </xsl:text>
</b></td><td>
<xsl:apply-templates select="unitid"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Call Phrase: </xsl:text></b></td><td>
<xsl:apply-templates select="unitid"/>

</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>

<!-- b01 edit - turn physloc off -->
<!--
<xsl:if test="physloc[string-length(text()|*)!=0]">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td><td>
<xsl:apply-templates select="physloc"/>
</td></tr>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">
<b><xsl:text>Location: </xsl:text></b></td><td>
<xsl:apply-templates select="physloc"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:if>
-->
<!-- End b01 edit -->

<xsl:if test="note[string-length(text()|*)!=0]">
<xsl:for-each select="note">
<xsl:choose>
<xsl:when test="@label">
<tr><td> </td><td valign="top">
<b><xsl:value-of select="@label"/>
</b></td></tr>
<xsl:for-each select="p">
<tr><td> </td><td valign="top">
<xsl:apply-templates/>
</td></tr>
</xsl:for-each>
</xsl:when>

<xsl:otherwise>
<tr><td> </td><td valign="top">

<!-- b01 edit - chg Location to Note, wrap with xsl-colon-text -->
<b><xsl:text>Note: </xsl:text></b></td><td>
<!-- End b01 edit -->

<xsl:apply-templates select="note"/>
</td></tr>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:if>
             
</table>
<hr></hr>

</xsl:for-each>
</xsl:template>

<!--This template rule formats the top-level bioghist element. -->
<xsl:template name="archdesc-bioghist">
<xsl:variable name="file">
<xsl:value-of select="ead/eadheader/eadid"/>
</xsl:variable>

<xsl:if test="ead/archdesc/bioghist[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/bioghist">
<xsl:apply-templates/>
<hr></hr>
</xsl:for-each>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/head">
<h3><a name="a2">
<xsl:apply-templates/>
</a></h3>
</xsl:template>

<xsl:template match="ead/archdesc/bioghist/p">
<p style="margin-left: 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>

<!--<xsl:template match="ead/archdesc/bioghist/chronlist">
<xsl:apply-templates/>
</xsl:template>-->

<xsl:template match="ead/archdesc/bioghist/bioghist">
<h3>
<xsl:apply-templates select="head"/>
</h3>
<xsl:for-each select="p">
<p style="margin-left: 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:template>

<!--This template rule formats a chronlist element. -->
<xsl:template match="*/chronlist">

<table width="100%" cellpadding="3">
<tr><td width="5%"> </td><td width="30%"> </td>
<td width="65%"> </td></tr>

<xsl:for-each select="head">
<xsl:apply-templates/>
</xsl:for-each>

<xsl:for-each select="listhead">
<tr><td><b>
<xsl:apply-templates select="head01"/>
</b></td>
<td><b>
<xsl:apply-templates select="head02"/>
</b></td></tr>
</xsl:for-each>

<xsl:for-each select="chronitem">
<tr><td></td><td valign="top">
<xsl:apply-templates select="date"/>
</td>
<td valign="top">

<!--
<xsl:apply-templates select="event"/>
-->
<xsl:for-each select="event | eventgrp/event">
<xsl:apply-templates/><br></br>
</xsl:for-each>

</td></tr>
</xsl:for-each>
</table>
</xsl:template>

<!--This template rule formats the scopecontent element. -->
<xsl:template name="archdesc-scopecontent">
<xsl:if test="ead/archdesc/scopecontent[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/scopecontent"> 
<xsl:apply-templates/>
</xsl:for-each>
<hr></hr>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/scopecontent/head">
<h3><a name="a3">
<xsl:apply-templates/>
</a></h3>
</xsl:template>

<!-- This formats an arrangement list embedded in a scope content statement. 
<xsl:template match="ead/archdesc/scopecontent/arrangement">
<xsl:for-each select="p">
<p style="margin-left: 30pt">
<xsl:apply-templates/>
</p>
</xsl:for-each>
<xsl:for-each select="list">
<xsl:for-each select="item">
<p style="margin-left: 60pt">
<a><xsl:attribute name="href">#series<xsl:number/>
</xsl:attribute>
<xsl:apply-templates select="."/>
</a>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:template>-->

<xsl:template match="ead/archdesc/scopecontent/p">
<p style="margin-left: 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>


<!--This template rule formats the arrangement element. -->
<xsl:template name="archdesc-arrangement">
<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/arrangement">
<table width="100%">
<tr><td width="5%"> </td><td width="5%"> </td>
<td width="90%"> </td></tr>

<tr><td colspan="3"> <h3><a name="a5">
<xsl:apply-templates select="head"/>
</a></h3>
</td></tr>

<xsl:for-each select="p">
<tr><td> </td><td colspan="2">
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
<xsl:for-each select="list">
<tr><td> </td><td colspan="2">
<xsl:apply-templates select="head"/>
</td></tr>
<xsl:for-each select="item">
<tr><td> </td><td> </td><td colspan="1">
<a><xsl:attribute name="href">#series<xsl:number/>
</xsl:attribute>
<xsl:apply-templates select="."/>
</a>
</td></tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level arrangement element. 
<xsl:template name="archdesc-arrangement">
<xsl:if test="ead/archdesc/arrangement[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/arrangement">
<h3><a name="a5">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>-->


<!--This template rule formats the top-level relatedmaterial element. -->
<xsl:template name="archdesc-relatedmaterial">
<xsl:if test="ead/archdesc/relatedmaterial[string-length(text()|*)!=0]">
<h3><a name="a6">
<b><xsl:apply-templates select="ead/archdesc/relatedmaterial/head"/></b></a></h3>
<xsl:for-each select="ead/archdesc/relatedmaterial">
<xsl:apply-templates select="*[not(self::head)]"/> 
</xsl:for-each>
<hr></hr>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/relatedmaterial/p">
<p style="margin-left : 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>


<!--This template rule formats the top-level separatedmaterial element. -->

<xsl:template name="archdesc-separatedmaterial">
<xsl:if test="//separatedmaterial">
<h3><a name="a7">
<b><xsl:apply-templates select="//separatedmaterial/head"/></b></a></h3>
<xsl:for-each select="ead/archdesc/separatedmaterial">
<xsl:apply-templates select="*[not(self::head)]"/> 
</xsl:for-each>
<hr></hr>
</xsl:if>
</xsl:template>

<xsl:template match="ead/archdesc/separatedmaterial/p">
<p style="margin-left : 30pt">
<xsl:apply-templates/>
</p>
</xsl:template>


<!--This template rule formats the top-level otherfindaid element. -->
<xsl:template name="archdesc-otherfindaid">
<xsl:if test="ead/archdesc/otherfindaid[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/otherfindaid">
<h3><a name="a8">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
<p>

</p>
</xsl:for-each>
<hr></hr>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level index element. -->
<xsl:template name="archdesc-index">
<xsl:if test="ead/archdesc/index[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/index">
<h3><a name="a9">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>

<!--This template rule formats the top-level bibliography element. -->
<xsl:template name="archdesc-bibliography">
<xsl:if test="ead/archdesc/bibliography[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/bibliography">
<h3><a name="a10">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<xsl:for-each select="bibref">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
<hr></hr>
</xsl:if>
</xsl:template>



<!--This template rule formats the top-level odd element. -->
<xsl:template name="archdesc-odd">
<xsl:if test="ead/archdesc/odd[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/odd">
<h3><a name="a11">
<b><xsl:apply-templates select="head"/>
</b></a></h3>
<xsl:for-each select="p">
<p style="margin-left : 30pt"> 
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>


<xsl:template name="archdesc-control">
<xsl:if test="ead/archdesc/controlaccess[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/controlaccess">
<table width="100%">
<tr><td width="5%"> </td><td width="5%"> </td>
<td width="90%"> </td></tr>

<tr><td colspan="3"><h3><a name="a12">
<xsl:apply-templates select="head"/>
</a></h3> </td></tr>

<tr><td> </td><td colspan="2">
<xsl:apply-templates select="p"/>
</td></tr>
    
<xsl:for-each select="./controlaccess">
<tr><td> </td><td colspan="2"><b>
<xsl:apply-templates select="head"/>
</b></td></tr>

<!-- b01 edit - add name -->
<xsl:for-each select="name | subject | corpname | persname | genreform | 
famname | title | geogname | occupation">
<!-- End b01 edit -->

<xsl:sort select="."/>
<tr><td></td><td></td><td>
<xsl:apply-templates select="."/>
</td></tr>
</xsl:for-each>
</xsl:for-each>
</table>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>


<!--This template rule formats a top-level accessrestrict element. -->
<xsl:template name="archdesc-restrict">
<xsl:if test="ead/archdesc/accessrestrict[string-length(text()|*)!=0] | ead/archdesc/userestrict[string-length(text()|*)!=0]">
<h3>
<a name="a14">
<b><xsl:text>Restrictions</xsl:text>
</b></a></h3>
<xsl:for-each select="ead/archdesc/accessrestrict">
<h4 style="margin-left : 15pt"><b><xsl:value-of select="head"/></b></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/userestrict">
<h4 style="margin-left : 15pt"><b><xsl:value-of select="head"/></b></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>

<!-- all the children of what used to be admininfo will be processed here -->

<xsl:template name="archdesc-admininfo">
<xsl:if test="ead/archdesc/prefercite[string-length(text()|*)!=0] | ead/archdesc/custodhist[string-length(text()|*)!=0] | 
ead/archdesc/altformavail[string-length(text()|*)!=0] |
ead/archdesc/acqinfo[string-length(text()|*)!=0] | 
ead/archdesc/processinfo[string-length(text()|*)!=0] | ead/archdesc/appraisal[string-length(text()|*)!=0] | 
ead/archdesc/accruals[string-length(text()|*)!=0]">
<h3><a name="a13">
<xsl:text>Administrative Information</xsl:text>
</a></h3>
<xsl:call-template name="archdesc-custodhist"/>
<xsl:call-template name="archdesc-altform"/>
<xsl:call-template name="archdesc-prefercite"/>
<xsl:call-template name="archdesc-acqinfo"/>
<xsl:call-template name="archdesc-processinfo"/>
<xsl:call-template name="archdesc-appraisal"/>
<xsl:call-template name="archdesc-accruals"/>
<p>

</p>
<hr></hr>
</xsl:if>
</xsl:template>


<!--This template rule formats a top-level custodhist element. -->
<xsl:template name="archdesc-custodhist">
<xsl:if test="ead/archdesc/custodhist[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/custodhist">
<h4 style="margin-left : 15pt">
<a name="a16">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>


<!--This template rule formats a top-level altformavailable element. -->
<xsl:template name="archdesc-altform">
<xsl:if test="ead/archdesc/altformavailable[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/altformavailable">
<h4 style="margin-left : 15pt">
<a name="a17">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>



<!--This template rule formats a top-level prefercite element. -->
<xsl:template name="archdesc-prefercite">
<xsl:if test="ead/archdesc/prefercite[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/prefercite">
<h4 style="margin-left : 15pt">
<a name="a18">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>



<!--This template rule formats a top-level acqinfo element. -->
<xsl:template name="archdesc-acqinfo">
<xsl:if test="ead/archdesc/acqinfo[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/acqinfo">
<h4 style="margin-left : 15pt"> 
<a name="a19">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level procinfo element. -->
<xsl:template name="archdesc-processinfo">
<xsl:if test="ead/archdesc/processinfo[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/processinfo">
<h4 style="margin-left : 15pt">
<a name="a20">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level appraisal element. -->
<xsl:template name="archdesc-appraisal">
<xsl:if test="ead/archdesc/appraisal[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/appraisal">
<h4 style="margin-left : 15pt"> 
<a name="a21">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 30pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!--This template rule formats a top-level accruals element. -->
<xsl:template name="archdesc-accruals">
<xsl:if test="ead/archdesc/accruals[string-length(text()|*)!=0]">
<xsl:for-each select="ead/archdesc/accruals">
<h4 style="margin-left : 15pt">
<a name="a22">
<b><xsl:apply-templates select="head"/>
</b></a></h4>
<xsl:for-each select="p">
<p style="margin-left : 25pt">
<xsl:apply-templates select="."/>
</p>
</xsl:for-each>
</xsl:for-each>
</xsl:if>
</xsl:template>

<!-- dsc  This is all new territory -->
<xsl:template name="dsc">
<xsl:for-each select="ead/archdesc/dsc">
<h3><a name="a23">
<xsl:apply-templates select="head"/>
</a></h3>

<xsl:if test="child::p">
<p style="margin-left: 25 pt"><i>
<xsl:apply-templates select="p"/>
</i></p>
</xsl:if>
</xsl:for-each>

<xsl:for-each select="ead/archdesc/dsc/c01">
<table cellpadding="5" border="0">

<tr>
<td colspan="4">
<a><xsl:attribute name="name">series<xsl:number/></xsl:attribute>&#x00A0;</a>
<p class="h2"><b><xsl:apply-templates select="did/unittitle"/></b></p>
</td>
</tr>

<tr><td colspan="4"><p class="seriesabstract"><xsl:apply-templates select="did/abstract"/></p></td></tr>

<xsl:if test="./child::scopecontent">
<tr><td colspan="4"><p class="h2"><b>Scope and Content: </b></p>
<xsl:for-each select="scopecontent/p">
<p style="margin-left: 30pt"><xsl:apply-templates select="."/></p>
</xsl:for-each>
</td></tr>
</xsl:if>

<xsl:variable name="subseriestitle">
<xsl:value-of select="c02/did/unittitle"/>
</xsl:variable>

<xsl:for-each select="c02">
<xsl:choose>
<xsl:when test="@level='subseries' or contains($subseriestitle, 'Subseries')">
   <tr><td colspan="4"><a><xsl:attribute name="name">subseries<xsl:number/></xsl:attribute>&#x00A0;</a>
<p class="h3subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
<xsl:if test="did/abstract">
   <tr><td colspan="4"><p class="seriesabstract"><xsl:apply-templates select="did/abstract"/></p></td></tr>
</xsl:if>
</xsl:when>

<xsl:otherwise>
<!-- do nothing be happy-->
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="scopecontent">
   <tr><td colspan="4"><p class="h3subseries"><b>Scope and Content: </b></p></td></tr>
<xsl:for-each select="p">
<tr><td colspan="4"><p><xsl:apply-templates/></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="did">
  <xsl:for-each select="container">
<xsl:variable name="box-number" select="self::container[@type='box']"/>
<!-- lots of ugly tree-climbing here but this determines in a succinct fashion whether to and how to render the container header -->
    <xsl:if test="@type='box'">
    <xsl:choose> 
    <xsl:when test="not(../../preceding-sibling::c02/did[container[@type='box']=$box-number]
    or ../preceding-sibling::c02/did[container[@type='box']=$box-number])
    or ../../preceding-sibling::c02/did[container[@type='volume']]">
       <xsl:if test="./following-sibling::container[@type='folder']">
    <xsl:copy-of select="$box-and-folder-heading"/>
       </xsl:if>
       <xsl:if test="./following-sibling::container[@type='volume']">
    <xsl:copy-of select="$box-and-volume-heading"/>
       </xsl:if>








<!--brian insert-->
	<xsl:if test="./following-sibling::container[@type='item']">
    <xsl:copy-of select="$box-and-item-heading"/>
       </xsl:if>
       <xsl:if test= "not(./following-sibling::container[@type='folder'] or ./following-sibling::container[@type='volume'] or ./following-sibling::container[@type='item'])">
    <xsl:copy-of select="$box-heading"/>
       </xsl:if>

       <!--xsl:if test="./following-sibling::container[@type='item']">
    <xsl:copy-of select="$box-and-item-heading"/>
       </xsl:if>
       <xsl:if test= "not(./following-sibling::container[@type='folder'] or ./following-sibling::container[@type='volume'] or ./following-sibling::container[@type='item'])">
    <xsl:copy-of select="$box-heading"/>
       </xsl:if>-->


<xsl:if test="./following-sibling::container[@type='folder']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<!--<xsl:if test="./following-sibling::container[@type='item']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td>
<td width="10%">&#x00A0;</td>
<td width="40%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='item']"/></td>


<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%" valign="top"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>-->

<!--Brian insert-->
<xsl:if test="./following-sibling::container[@type='item']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='item']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%" valign="top"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>




<xsl:if test="./following-sibling::container[@type='volume']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
       <xsl:if test= "not(./following-sibling::container)">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

</xsl:when> 

<xsl:otherwise> <!-- add no header (because they belong to the same box), but still number boxes, since we like repeated box numbers -->
<!-- to remove repeated box numbers replace the first td's contents with the hex for a nbsp -->
<xsl:if test="./following-sibling::container[@type='folder']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<!--Brian insert-->
<xsl:if test="./following-sibling::container[@type='item']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='item']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%" valign="top"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>


<xsl:if test="./following-sibling::container[@type='volume']">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<!-- test to see if it is a freestanding box without any folders etc within -->
       <xsl:if test= "not(./following-sibling::container)">
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:otherwise>
</xsl:choose>



<xsl:for-each select="note">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:if test="materialspec[string-length(text()|*)!=0]">
  <xsl:for-each select="materialspec">
    <tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><b><xsl:value-of select="@label"/> :</b>
<xsl:for-each select="p">
<xsl:apply-templates select="."/>
  </xsl:for-each>
</td></tr>
  </xsl:for-each>
</xsl:if>

    </xsl:if> <!-- end it is a box with whatever contents-->

<!-- here test to see whether we have a free-standing volume or reel or folder with various contents -->
<xsl:if test="@type='volume' and following-sibling='item' and not(./preceding-sibling::container[@type='box'])">
    <xsl:copy-of select="$volume-and-item-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::item"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='volume' and not(./preceding-sibling::container[@type='box'])">
    <xsl:copy-of select="$volume-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='album' and not(./preceding-sibling::container[@type='box'])">
    <xsl:copy-of select="$album-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='album']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='reel' and not(./preceding-sibling::container[@type='box'])">
    <xsl:copy-of select="$reel-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='reel']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='map-case' and not(./preceding-sibling::container[@type='box'])">
    <xsl:copy-of select="$map-case-and-drawer-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='map-case']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='folder' and not(./preceding-sibling::container[@type='box'] or ./preceding-sibling::container[@type='volume'])">
    <xsl:copy-of select="$folder-heading"/>
      <tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='folder']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:for-each>

<xsl:for-each select="note">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:if test="materialspec[string-length(text()|*)!=0]">
  <xsl:for-each select="materialspec">
    <tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><b><xsl:value-of select="@label"/> :</b>
<xsl:for-each select="p">
<xsl:apply-templates select="."/>
  </xsl:for-each>
</td></tr>
  </xsl:for-each>
</xsl:if>

</xsl:for-each>

<!-- c03 -->

<xsl:for-each select="c03">
<a><xsl:attribute name="name">c03-<xsl:number/></xsl:attribute></a>
<xsl:variable name="subsubseriestitle">
<xsl:value-of select="c03/did/unittitle"/>
</xsl:variable>

<xsl:choose>
<xsl:when test="@level='otherlevel' and child::c04">
<tr><td colspan="4"><p class="h2subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
</xsl:when>
<xsl:when test="@level='otherlevel' and not(child::container)">
  <tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td width="40%"><p><xsl:apply-templates select="did/unittitle"/></p></td><td width="40%">&#x00A0;</td></tr>
</xsl:when>
<xsl:when test="@level='otherlevel' and child::container">
<tr><td colspan="4"><p class="h3subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
</xsl:when>

<xsl:otherwise>
<!-- do nothing be happy-->
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="scopecontent">
<tr><td colspan="4"><p class="h3subseries"><b>Scope and Content: </b></p></td></tr>
<xsl:for-each select="p">
<tr><td colspan="4"><p><xsl:apply-templates/></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="did">
<xsl:for-each select="container">
<xsl:variable name="c03-box-number" select="self::container[@type='box']"/>
<xsl:if test="@type='box'">
<xsl:choose> 
<xsl:when test="not(../../preceding-sibling::c03/did[container[@type='box']=$c03-box-number]
or ../preceding-sibling::c03/did[container[@type='box']=$c03-box-number])
or ../../preceding-sibling::c03/did[container[@type='volume']]">
<xsl:if test="./following-sibling::container[@type='folder']">
<xsl:copy-of select="$box-and-folder-heading"/>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='volume']">
<xsl:copy-of select="$box-and-volume-heading"/>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='item']">
<xsl:copy-of select="$box-and-item-heading"/>
</xsl:if>
<xsl:if test= "not(./following-sibling::container[@type='folder'] or ./following-sibling::container[@type='volume'] or ./following-sibling::container[@type='item'])">
<xsl:copy-of select="$box-heading"/>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='folder']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td>
<td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='item']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td>
<td width="10%">&#x00A0;</td>
<td width="40%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='item']"/></td>
<!--<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>-->
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='volume']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<xsl:if test= "not(./following-sibling::container)">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:when> 

<xsl:otherwise> <!-- add no header (because they belong to the same box), but still number boxes, since we like repeated box numbers -->
<!-- to remove repeated box numbers replace the first td's contents with the hex for a nbsp -->
<xsl:if test="./following-sibling::container[@type='folder']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='volume']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<!-- test to see if it is a freestanding box without any folders etc within -->
<xsl:if test= "not(./following-sibling::container)">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:if> <!-- end it is a box with whatever contents-->

<!-- here test to see whether we have a free-standing volume or reel or folder with various contents -->
<xsl:if test="@type='volume' and following-sibling='item' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$volume-and-item-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::item"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='volume' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$volume-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='album' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$album-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='album']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='reel' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$reel-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='reel']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='map-case' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$map-case-and-drawer-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='map-case']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='folder' and not(./preceding-sibling::container[@type='box'] or ./preceding-sibling::container[@type='volume'])">
<xsl:copy-of select="$folder-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='folder']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/>
</td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/>
</td></tr>
</xsl:if>
</xsl:for-each>  <!-- container -->

<xsl:for-each select="note">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="abstract">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><xsl:apply-templates select="."/></p></td></tr>
</xsl:for-each>

</xsl:for-each>

<!-- c04 -->

<xsl:for-each select="c04">
<a><xsl:attribute name="name">c04-<xsl:number/></xsl:attribute></a>
<xsl:variable name="subsubsubseriestitle">
<xsl:value-of select="c04/did/unittitle"/>
</xsl:variable>

<xsl:choose>
<xsl:when test="@level='otherlevel' and child::c05">
<tr><td colspan="4"><p class="h2subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
</xsl:when>
<xsl:when test="@level='otherlevel' and not(child::container)">
  <tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td width="40%"><p><xsl:apply-templates select="did/unittitle"/></p></td><td width="40%">&#x00A0;</td></tr>
</xsl:when>
<xsl:when test="@level='otherlevel' and child::container">
<tr><td colspan="4"><p class="h3subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
</xsl:when>

<!--<xsl:choose>
<xsl:when test="@level='otherlevel'">
<tr><td colspan="4"><p class="h3subseries"><b><xsl:apply-templates select="did/unittitle"/></b></p></td></tr>
</xsl:when>-->

<xsl:otherwise>
<!-- do nothing be happy-->
</xsl:otherwise>
</xsl:choose>

<xsl:for-each select="scopecontent">
<tr><td colspan="4"><p class="h3subseries"><b>Scope and Content: </b></p></td></tr>
<xsl:for-each select="p">
<tr><td colspan="4"><p><xsl:apply-templates/></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="did">
  <xsl:for-each select="container">
<xsl:variable name="c04-box-number" select="self::container[@type='box']"/>

<xsl:if test="@type='box'">
<xsl:choose> 
<xsl:when test="not(../../preceding-sibling::c04/did[container[@type='box']=$c04-box-number]
or ../preceding-sibling::c04/did[container[@type='box']=$c04-box-number])
or ../../preceding-sibling::c04/did[container[@type='volume']]">
<xsl:if test="./following-sibling::container[@type='folder']">
<xsl:copy-of select="$box-and-folder-heading"/>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='volume']">
<xsl:copy-of select="$box-and-volume-heading"/>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='item']">
<xsl:copy-of select="$box-and-item-heading"/>
</xsl:if>
<xsl:if test= "not(./following-sibling::container[@type='folder'] or ./following-sibling::container[@type='volume'] or ./following-sibling::container[@type='item'])">
<xsl:copy-of select="$box-heading"/>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='folder']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td>
<td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='item']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td>
<td width="10%">&#x00A0;</td>
<td width="40%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='item']"/></td>
<!--<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>-->
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="./following-sibling::container[@type='volume']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<xsl:if test= "not(./following-sibling::container)">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:when> 

<xsl:otherwise> <!-- add no header (because they belong to the same box), but still number boxes, since we like repeated box numbers -->
<!-- to remove repeated box numbers replace the first td's contents with the hex for a nbsp -->
<xsl:if test="./following-sibling::container[@type='folder']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='folder']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<xsl:if test="./following-sibling::container[@type='volume']">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='volume']"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td>
<td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
<!-- test to see if it is a freestanding box without any folders etc within -->
<xsl:if test= "not(./following-sibling::container)">
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='box']"/></td><td width="10%" valign="top">&#x00A0;</td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:if> <!-- end it is a box with whatever contents-->

<!-- here test to see whether we have a free-standing volume or reel or folder with various contents -->
<xsl:if test="@type='volume' and following-sibling='item' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$volume-and-item-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::item"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='volume' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$volume-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='volume']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='album' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$album-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='album']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='reel' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$reel-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='reel']"/></td><td width="10%" valign="top"></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='map-case' and not(./preceding-sibling::container[@type='box'])">
<xsl:copy-of select="$map-case-and-drawer-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='map-case']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

<xsl:if test="@type='folder' and not(./preceding-sibling::container[@type='box'] or ./preceding-sibling::container[@type='volume'])">
<xsl:copy-of select="$folder-heading"/>
<tr><td width="10%" valign="top"><xsl:apply-templates select="self::container[@type='folder']"/></td><td width="10%" valign="top"><xsl:apply-templates select="following-sibling::container[@type='drawer']"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unittitle"/></td><td width="40%"><xsl:apply-templates select="following-sibling::unitdate"/></td></tr>
</xsl:if>

</xsl:for-each> <!-- container -->

<xsl:for-each select="note">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

<xsl:for-each select="abstract">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><xsl:apply-templates select="."/></p></td></tr>
</xsl:for-each>

</xsl:for-each> <!-- did -->

<xsl:for-each select="phystech">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>

<xsl:for-each select="phystech">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>

<xsl:for-each select="phystech">
<xsl:for-each select="p">
<tr><td width="10%">&#x00A0;</td><td width="10%">&#x00A0;</td><td colspan="2"><p><i><xsl:apply-templates select="."/></i></p></td></tr>
</xsl:for-each>
</xsl:for-each>

</xsl:for-each>

<tr><td colspan="4"><hr /></td></tr>
</table>

<a href="#a0">Return to the Top of Page</a>

</xsl:for-each>
</xsl:template>
</xsl:stylesheet>


<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Stylesheet created 19 October 2004 by Vikram Ahmed, ECU -->
<!-- styles below used in both eadbase.xsl and eadframes.xsl -->

<!-- named templates -->

<!-- Variable to store the value of the border of declared tables-->
<xsl:variable name="borderon">0</xsl:variable>

<!-- Table outline template -->
<xsl:template name="tableattrs">
	<xsl:attribute name="border">
		<xsl:value-of select="$borderon"/>
	</xsl:attribute>
	<xsl:attribute name="cellspacing">2</xsl:attribute>
	<xsl:attribute name="cellpadding">4</xsl:attribute>
</xsl:template>

<!-- Table outline template No.2-->
<xsl:template name="tableattrs1">
	<xsl:attribute name="border">
		<xsl:value-of select="$borderon"/>
	</xsl:attribute>
	<xsl:attribute name="cellspacing">0</xsl:attribute>
	<xsl:attribute name="cellpadding">0</xsl:attribute>
</xsl:template>

<!--Sequence of unittitle, unitdate, unitid and physdesc with white space in between-->
<xsl:template name="component-did">
	<xsl:if test="unitid">
	<xsl:apply-templates select="unitid"/>
<xsl:text> </xsl:text>
	</xsl:if>
	<xsl:apply-templates select="unittitle"/>
	<xsl:if test="unittitle and unitdate">
		<xsl:text> </xsl:text>		
	</xsl:if>
	<xsl:apply-templates select="unitdate"/>
	<xsl:if test="physdesc">
		<br/>
	</xsl:if>

	<xsl:apply-templates select="physdesc/extent"/>
</xsl:template>


<!--generate linking ids in toc-->
<xsl:template name="addidtoc">
	<xsl:attribute name="name">
		<xsl:choose>
			<xsl:when test="@name">
				<xsl:value-of select="@name"/>
			</xsl:when>
	 		<xsl:otherwise>
				<xsl:value-of select="generate-id()"/>
			</xsl:otherwise>
		</xsl:choose>			
	</xsl:attribute>
</xsl:template>

<!--generate linking ids in body-->
<xsl:template name="addid">
	<xsl:if test="@name">
		<xsl:attribute name="name">
			<xsl:value-of select="@name"/>
		</xsl:attribute>
	</xsl:if>
</xsl:template>

<!-- the text of the finding aid -->

<xsl:template match="archdesc">
<xsl:element name="hr"/>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="archdesc//*[head]            [not(self::did | self::scopecontent |               self::chronlist |                  self::controlaccess |               self::dsc | self::relatedmaterial |               self::c01 | self::separatedmaterial |               self::c02 | self::bibliography |               self::list)]">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>		
		<xsl:element name="br"/>
                <xsl:call-template name="back"/>
	</xsl:element>
</xsl:template>

<xsl:template match="archdesc//*[head]            [(self::scopecontent[1])]">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>		
		<xsl:element name="br"/>
	</xsl:element>
</xsl:template>

<xsl:template match="archdesc//*[head]            [(self::scopecontent[2])]">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>		
		<xsl:element name="br"/>
		<xsl:call-template name="back"/>
	</xsl:element>
</xsl:template>

<xsl:template match="archdesc/*/head[not(parent::list)]">
	<xsl:element name="h2">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>
		
</xsl:element>
</xsl:template>

<xsl:template match="archdesc/*/*/head[not(parent::list)]">
	<xsl:element name="h3">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="archdesc/*/*/*/head[not(parent::list)]">
	<xsl:element name="h4">
	<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<!-- top level did -->

<xsl:template match="archdesc/did">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>		
		<xsl:element name="table">
			<xsl:attribute name="cellspacing">4</xsl:attribute>
			<xsl:call-template name="tableattrs"/>
			<xsl:for-each select="head">
				<xsl:element name="h2">
						<xsl:apply-templates/>
				</xsl:element>
			</xsl:for-each>

<!-- templates for children of did -->
			<xsl:for-each select="unittitle |                              repository | langmaterial |                             abstract[1] |                              origination | container | physloc |                             unitdate">
				<xsl:element name="tr">
					<xsl:call-template name="addid"/>
					<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
						<xsl:attribute name="nowrap"/>
						<xsl:element name="b">
							<xsl:value-of select="@label"/>
							<xsl:text>:</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="td">
					<xsl:apply-templates/>
					<xsl:text> </xsl:text>
					<xsl:value-of select=" @unit"/>
					</xsl:element>
					
				</xsl:element>
			</xsl:for-each>
			
			<xsl:for-each select="abstract[2]">
				<xsl:element name="tr">
					<xsl:call-template name="addid"/>
					<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
						<xsl:attribute name="nowrap"/>
						
					</xsl:element>
					<xsl:element name="td">
					<xsl:apply-templates/>
					<xsl:text> </xsl:text>
					<xsl:value-of select=" @unit"/>
					</xsl:element>
					
				</xsl:element>
			</xsl:for-each>

			<xsl:for-each select="physdesc">
				<xsl:element name="tr">
					<xsl:call-template name="addid"/>
					<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
						<xsl:attribute name="nowrap"/>
						<xsl:element name="b">
							<xsl:value-of select="@label"/>
							<xsl:text>:</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="td">
					<xsl:apply-templates select="extent[1]"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="extent[1]/@unit"/>
					<xsl:text>, </xsl:text>
					<xsl:apply-templates select="extent[2]"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="extent[2]/@unit"/>
					</xsl:element>
					
				</xsl:element>
			</xsl:for-each>

		</xsl:element>
	</xsl:element>
<xsl:element name="br"/>
<xsl:call-template name="back"/>
</xsl:template>

<xsl:template match="persname[parent::origination] |           famname[parent::origination] |           corpname[parent::origination]">
	<xsl:if test="preceding-sibling::*">
		<xsl:element name="br"/>
	</xsl:if>
		<xsl:apply-templates/>
</xsl:template>

<!-- controlaccess templates -->
<xsl:template match="archdesc//controlaccess">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
			<xsl:apply-templates select="head"/>
			<xsl:for-each select="corpname | famname | geogname | name |                          occupation | persname | subject |                          genreform | function">
				<xsl:element name="p">
				<xsl:attribute name="style">margin-left:36px</xsl:attribute>
				<xsl:apply-templates select="."/>
			</xsl:element>
		</xsl:for-each>
		<xsl:apply-templates select="p | list | table | controlaccess"/>
	</xsl:element>
<xsl:element name="br"/>
<xsl:call-template name="back"/>
</xsl:template>

<!-- Other child elements of archdesc-->
<xsl:template match="archdesc//bibliography | archdesc//separatedmaterial | archdesc//relatedmaterial">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
			<xsl:apply-templates select="head"/>
			<xsl:for-each select="archref | bibref | archref">
				<xsl:element name="p">
				<xsl:attribute name="style">margin-left:36px</xsl:attribute>
				<xsl:apply-templates select="."/>
			</xsl:element>
		</xsl:for-each>
		<xsl:apply-templates select="p | list | table | controlaccess"/>
	</xsl:element>
</xsl:template>

<!-- dsc -->

<xsl:template match="archdesc/dsc">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
	<xsl:for-each select="*[not(self::c01)][not(self::thead)]">
		<xsl:apply-templates select="."/>
	</xsl:for-each>
	<xsl:apply-templates select="c01"/>
	</xsl:element>
<xsl:element name="br"/>
</xsl:template>

<!--c01 to c07 templates-->

<xsl:template match="c01">
	<table width="100%">
		<tr>
			<td width="10%"> </td><td width="10%"> </td>
			<td width="10%"> </td><td width="10%"> </td>
			<td width="10%"> </td><td width="10%"> </td>
			<td width="10%"> </td><td width="10%"> </td>
			<td width="10%"> </td><td width="10%"> </td>
		</tr>
		<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="c01/did">
	<tr>
	<td colspan="10">
		<b><xsl:call-template name="component-did"/></b>
	</td>
	</tr>
</xsl:template>

<xsl:template match="c02/did">
	<tr>
	<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>
<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[2]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>	</td>
		<td colspan="8">
		<xsl:call-template name="component-did"/>
                <xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>

<xsl:template match="c03/did">
	<tr>
		<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>	<td>
		<xsl:apply-templates select="container[2]"/>
	</td>
	<td> </td>
	<td colspan="7">
		<xsl:call-template name="component-did"/>
		<xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>

<xsl:template match="c04/did">
	<tr>
		<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>	<td>
		<xsl:apply-templates select="container[2]"/>
	</td>
	<td> </td><td> </td>
	<td colspan="6">
		<xsl:call-template name="component-did"/>
                <xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>

<xsl:template match="c05/did">
	<tr>
	<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>
	<td>
		<xsl:apply-templates select="container[2]"/>
	</td>
	<td> </td><td> </td><td> </td>
	<td colspan="5">
		<xsl:call-template name="component-did"/>
                <xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>

<xsl:template match="c06/did">
	<tr>
	<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>	<td>
		<xsl:apply-templates select="container[2]"/>
	</td>
	<td> </td><td> </td><td> </td><td> </td>
	<td colspan="4">
		<xsl:call-template name="component-did"/>
                <xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>

<xsl:template match="c07/did">
	<tr>
	<td>
		<b>
<xsl:choose>
<xsl:when test="container[@type='folder']">
		<xsl:text>Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='box']">
		<xsl:text>Box </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='volume']">
		<xsl:text>Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiocassette']">
		<xsl:text>Audiocassette </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiodisc']">
		<xsl:text>Audiodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='audiotape']">
		<xsl:text>Audiotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='cd']">
		<xsl:text>CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='datacd']">
		<xsl:text>Data CD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='dvd']">
		<xsl:text>DVD </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='film']">
		<xsl:text>Film </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='image']">
		<xsl:text>Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='imagefolder']">
		<xsl:text>Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='interview']">
		<xsl:text>Interview </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='museumitem']">
		<xsl:text>Museum Item </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='notebook']">
		<xsl:text>Notebook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ocd']">
		<xsl:text>Optical Disc Cartridge </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimage']">
		<xsl:text>Oversize Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='oimagefolder']">
		<xsl:text>Oversize Image Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaper']">
		<xsl:text>Oversize Paper </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='opaperfolder']">
		<xsl:text>Oversize Paper Folder </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='ovolume']">
		<xsl:text>Oversize Volume </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='photoalbum']">
		<xsl:text>Photograph Album </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='reel']">
		<xsl:text>Reel </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='scrapbook']">
		<xsl:text>Scrapbook </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='sfimage']">
		<xsl:text>Special Format Image </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videodisc']">
		<xsl:text>Videodisc </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:when test="container[@type='videotape']">
		<xsl:text>Videotape </xsl:text>
<xsl:apply-templates select="container[1]"/>
		</xsl:when>
<xsl:otherwise><xsl:apply-templates select="container[1]"/></xsl:otherwise>
</xsl:choose>
</b>
	</td>	<td>
		<xsl:apply-templates select="container[2]"/>
	</td>
	<td> </td><td> </td><td> </td><td> </td><td> </td>
	<td colspan="4">
		<xsl:call-template name="component-did"/>
                <xsl:if test="daogrp/daodesc/p">
		<xsl:call-template name="daodisplay"/> 
 		</xsl:if>
		<xsl:if test="daogrp/dao">
		<xsl:call-template name="daolink"/> 
 		</xsl:if>
                <xsl:if test="dao">
		<xsl:call-template name="daolink1"/> 
 		</xsl:if> 
	</td>
	</tr>
</xsl:template>


<!-- lists -->

<xsl:template match="list/head">
	<xsl:choose> 
		<xsl:when test="ancestor::titlepage">
			<xsl:element name="h3">
				<xsl:call-template name="addid"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:element name="h4">
				<xsl:call-template name="addid"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="listhead">
	<xsl:element name="tr">
		<xsl:call-template name="addid"/>
		<xsl:element name="th">
			<xsl:attribute name="align">
				<xsl:text>left</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="head01"/>
		</xsl:element>
		<xsl:element name="th">
			<xsl:attribute name="align">
				<xsl:text>left</xsl:text>
			</xsl:attribute>
				<xsl:apply-templates select="head02"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<xsl:template match="list[@type='simple'][not(parent::controlaccess)][not(parent::titlepage)]">
		<xsl:if test="parent::titlepage">
			<xsl:element name="br"/>
		</xsl:if>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head"/>
		<xsl:for-each select="item">
			<xsl:element name="tr">
				<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
							<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<xsl:template match="list[@type='simple'][(parent::titlepage)]">
		<xsl:element name="br"/>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs1"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head"/>
		<xsl:for-each select="item">
			<xsl:element name="tr">
				<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
							<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<xsl:template match="controlaccess[1]/list[@type='simple']">
		<xsl:if test="parent::titlepage">
			<xsl:element name="br"/>
		</xsl:if>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head"/>
		<xsl:for-each select="item">
			<xsl:element name="tr">
				<xsl:element name="td">
						<xsl:attribute name="valign">top</xsl:attribute>
							<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>


<xsl:template match="controlaccess[2]/list[@type='simple']">
		<xsl:for-each select="item">
				<xsl:element name="div">
					<xsl:attribute name="style">margin-left:12px</xsl:attribute>
<xsl:apply-templates/>
<xsl:element name="br"/>
				</xsl:element> 
		</xsl:for-each>
</xsl:template>

<xsl:template match="controlaccess[2]/list[@type='simple']/item/ref">
	<xsl:element name="div">
		<xsl:attribute name="style">margin-left:18px</xsl:attribute>
<xsl:apply-templates/>
	</xsl:element> <!-- end: div -->
</xsl:template>

<xsl:template match="controlaccess[1]/list[@type='simple']/item/ref">
	<xsl:element name="div">
		<xsl:attribute name="style">margin-left:18px</xsl:attribute>
<xsl:apply-templates/>
	</xsl:element> <!-- end: div -->
</xsl:template>


<xsl:template match="list[@type='deflist']">
		<xsl:if test="parent::titlepage">
			<xsl:element name="br"/>
		</xsl:if>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head | listhead"/>
		<xsl:for-each select="defitem">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="colspan">5</xsl:attribute>
				<xsl:attribute name="valign">top</xsl:attribute>
				<xsl:element name="b">
					<xsl:apply-templates select="label"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="td">
				<xsl:attribute name="colspan">15</xsl:attribute>
				<xsl:attribute name="valign">top</xsl:attribute>
				<xsl:apply-templates select="item"/>
			</xsl:element>
		</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<xsl:template match="list[@type=&quot;ordered&quot;]">
		<xsl:if test="parent::titlepage">
			<xsl:element name="br"/>
		</xsl:if>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head"/>
		<xsl:for-each select="item"> <!-- node is item -->
			<xsl:element name="tr">
				<xsl:element name="td">
					<xsl:attribute name="valign">
						<xsl:text>top</xsl:text>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="parent::list/@numeration">
							<xsl:choose>
								<xsl:when test="parent::list/@numeration=&quot;arabic&quot;">
									<xsl:number level="single" format="1."/>
								</xsl:when>
								<xsl:when test="parent::list/@numeration=&quot;upperalpha&quot;">
									<xsl:number level="single" format="A."/>
								</xsl:when>
								<xsl:when test="parent::list/@numeration=&quot;loweralpha&quot;">
									<xsl:number level="single" format="a."/>
								</xsl:when>
								<xsl:when test="parent::list/@numeration=&quot;upperroman&quot;">
									<xsl:number level="single" format="I."/>
								</xsl:when>
								<xsl:when test="parent::list/@numeration=&quot;lowerroman&quot;">
									<xsl:number level="single" format="i."/>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose> <!-- inner choose -->
								
								<xsl:when test="parent::list/parent::item/parent::list/parent::item/parent::list[not(parent::item)]">
									<xsl:number level="single" format="1."/>
								</xsl:when>
																<xsl:when test="parent::list/parent::item/parent::list[not(parent::item)]">
									<xsl:number level="single" format="A."/>
								</xsl:when>
								
								<xsl:when test="parent::list[not(parent::item)]">
									<xsl:number level="single" format="I."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:message>
										<xsl:text>List nesting exceeds level tested for in</xsl:text>
										<xsl:text>stylesheet!!!</xsl:text>
									</xsl:message>
								</xsl:otherwise>
							</xsl:choose> <!-- close inner choose -->
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
				</xsl:element>
				<xsl:element name="td">
					<xsl:attribute name="valign">
						<xsl:text>top</xsl:text>
					</xsl:attribute>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<xsl:template match="list[@type=&quot;marked&quot;]">
		<xsl:if test="parent::titlepage">
			<xsl:element name="br"/>
		</xsl:if>
		<xsl:if test="parent::p">
			<xsl:element name="br"/>
			<xsl:element name="br"/>
		</xsl:if>
	<xsl:element name="table">
		<xsl:call-template name="addidtoc"/>
		<xsl:call-template name="tableattrs"/>
		<xsl:if test="ancestor::list">
			<xsl:attribute name="style">margin-left:12px</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="head"/>
		<xsl:for-each select="item"> <!-- node is item -->
			<xsl:element name="tr">
				<xsl:element name="td">
					<xsl:attribute name="valign">
						<xsl:text>top</xsl:text>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="parent::list/@mark">
							<xsl:value-of select="parent::list/@mark"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>&#8226;</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
				<xsl:element name="td">
					<xsl:attribute name="valign">
						<xsl:text>top</xsl:text>
					</xsl:attribute>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
</xsl:template>

<!-- chronological list -->

<xsl:template match="archdesc//chronlist">
	<xsl:element name="a">
		<xsl:call-template name="addidtoc"/>
		<xsl:apply-templates select="head"/>
	<xsl:element name="table">
		<xsl:call-template name="tableattrs"/>
		<xsl:attribute name="width">100%</xsl:attribute>
		<xsl:apply-templates select="listhead"/>
		<xsl:for-each select="chronitem">
			<xsl:element name="tr">
				<xsl:element name="td">
					<xsl:attribute name="valign">top</xsl:attribute>
					<xsl:attribute name="nowrap"/>
					<xsl:apply-templates select="date"/>
				</xsl:element>
				<xsl:element name="td">
					<xsl:attribute name="valign">top</xsl:attribute>
					<xsl:apply-templates select="event"/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
	</xsl:element>
</xsl:template>

<xsl:template match="p[not(parent::titlepage)]         [not(parent::scopecontent/ancestor::c01)]         [not(parent::accessrestrict/ancestor::dsc)]          [not(parent::userestrict/ancestor::dsc)]          [not(parent::processinfo/ancestor::dsc)]">
	<xsl:element name="p">
		<xsl:call-template name="addid"/>
		<xsl:attribute name="style">margin-left:12px; text-indent:24px</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="dsc/accessrestrict/p | dsc/userestrict/p | dsc/processinfo/p">
	<xsl:element name="p">
		<xsl:call-template name="addid"/>
		<xsl:attribute name="style">margin-left:12px; text-indent:24px</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="titlepage/p">
	<xsl:element name="p">
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="c01/scopecontent/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c01/arrangement/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c01/accessrestrict/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c01/userestrict/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c01/altformavail/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c01/processinfo/p">
	<tr>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>



<xsl:template match="c02/scopecontent/p">
	<tr>
	<td>
	</td>
	<td colspan="9" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c02/arrangement/p">
	<tr>
<td>
</td>
	<td colspan="10" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c02/accessrestrict/p">
	<tr>
<td/>
	<td colspan="9" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c02/userestrict/p">
	<tr>
<td/>
	<td colspan="9" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c02/altformavail/p">
	<tr>
<td/>
	<td colspan="9" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c02/processinfo/p">
	<tr>
<td/>
	<td colspan="9" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>

<xsl:template match="c03/scopecontent/p">
	<tr>
	<td>
	</td>
<td/>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c03/arrangement/p">
	<tr>
	<td>
	</td>
<td/>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c03/accessrestrict/p">
	<tr>
<td> </td><td> </td>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c03/userestrict/p">
	<tr>
<td/>
<td/>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c03/altformavail/p">
	<tr>
<td/>
<td/>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c03/processinfo/p">
	<tr>
<td/>
<td/>
	<td colspan="8" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/scopecontent/p">
	<tr>
	<td>
	</td>
<td/>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/arrangement/p">
	<tr>
	<td>
	</td>
<td/>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/accessrestrict/p">
	<tr>
<td> </td><td> </td>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/userestrict/p">
	<tr>
<td/>
<td/>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/altformavail/p">
	<tr>
<td/>
<td/>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c04/processinfo/p">
	<tr>
<td/>
<td/>
<td/>
	<td colspan="7" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>

<xsl:template match="c05/scopecontent/p">
	<tr>
	<td>
	</td>
<td/>
<td/>
<td/>
	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c05/arrangement/p">
	<tr>
	<td>
	</td>
<td/>
<td/>
<td/>
	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c05/accessrestrict/p">
	<tr>
<td> </td><td> </td>
<td/>
<td/>
	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c05/userestrict/p">
	<tr>
<td/>
<td/><td/>
<td/>

	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c05/altformavail/p">
	<tr>
<td/>
<td/>
<td/>
<td/>
	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="c05/processinfo/p">
	<tr>
<td/>
<td/>
<td/>
<td/>
	<td colspan="6" style="text-indent:25pt">
	<xsl:apply-templates/>
	</td>
	</tr>
</xsl:template>
<xsl:template match="lb">
	<xsl:element name="br"/>
</xsl:template>

<xsl:template match="*">
	<xsl:choose>
		<xsl:when test="@name">
			<xsl:element name="a">
				<xsl:attribute name="name">
					<xsl:value-of select="@name"/>
				</xsl:attribute>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- linking elements -->

<xsl:template name="thisorthat">
	<xsl:choose>
		<xsl:when test="@href and @entityref">
			<xsl:value-of select="@href"/>
		</xsl:when>
		<xsl:when test="@entityref">
			<xsl:value-of select="unparsed-entity-uri(@entityref)"/>
		</xsl:when>
		<xsl:when test="@href">
			<xsl:value-of select="@href"/>
		</xsl:when>
		<xsl:when test="@entityref">
			<xsl:value-of select="unparsed-entity-uri(@entityref)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:message>
				<xsl:text>Unable to create link. No @entityref or @href on </xsl:text>
				<xsl:value-of select="local-name()"/>
				<xsl:text>.</xsl:text>
			</xsl:message>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--External Objects-->
<xsl:template match="extptr">
	<xsl:element name="p"/>
	<xsl:element name="img">
		<xsl:attribute name="src">
			<xsl:call-template name="thisorthat"/>
		</xsl:attribute>
		<xsl:attribute name="border">0</xsl:attribute>
	</xsl:element>
	<xsl:element name="br">
	</xsl:element>
</xsl:template>


<xsl:template match="defitem/item/extptr">
	<xsl:element name="p"/>
	<xsl:element name="img">
		<xsl:attribute name="src">
			<xsl:call-template name="thisorthat"/>
		</xsl:attribute>
		<xsl:attribute name="border">0</xsl:attribute>
	</xsl:element>
	<xsl:element name="br">
	</xsl:element>
</xsl:template>

<!--External Links-->
<xsl:template match="extref">
	<xsl:element name="a">
		<xsl:attribute name="style">
			<xsl:text>font-weight: bold; text-decoration:none;color:</xsl:text>
			<xsl:value-of select="$linkcolor"/>
		</xsl:attribute>
		<xsl:attribute name="href">
			<xsl:call-template name="thisorthat"/>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="@show=&quot;new&quot;">
			
<!-- 		<xsl:if test='@show="new"'> -->
					<xsl:attribute name="target">new</xsl:attribute>
<!-- 		</xsl:if> -->
				</xsl:when>
				<xsl:otherwise>
				<xsl:attribute name="target">_top</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<!--Text Formatting Templates-->

<xsl:template name="render">
		<xsl:choose>
		<xsl:when test="@render='italic'">
			<xsl:element name="i">
				<xsl:call-template name="addid"/>
				<xsl:apply-templates/>
			</xsl:element>
			</xsl:when>
			<xsl:when test="@render='bold'">
				<xsl:element name="b">
					<xsl:call-template name="addid"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@render='bolditalic'">
				<xsl:element name="b">
					<xsl:call-template name="addid"/>
					<xsl:element name="i">
						<xsl:apply-templates/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@render='boldquoted'">
				<xsl:element name="b">
					<xsl:call-template name="addid"/>
					<xsl:text>"</xsl:text>
					<xsl:apply-templates/>
					<xsl:text>"</xsl:text>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@render='boldunderline'">
				<xsl:element name="b">
					<xsl:call-template name="addid"/>
					<xsl:element name="u">
						<xsl:apply-templates/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@render='underline'">
				<xsl:element name="u">
					<xsl:call-template name="addid"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@render='quoted'">
				<xsl:element name="span">
					<xsl:call-template name="addid"/>
					<xsl:text>"</xsl:text>
					<xsl:apply-templates/>
					<xsl:text>"</xsl:text>
				</xsl:element>
			</xsl:when>
<xsl:when test="@render='doublequote'">
				<xsl:element name="span">
					<xsl:call-template name="addid"/>
					<xsl:text>"</xsl:text>
					<xsl:apply-templates/>
					<xsl:text>"</xsl:text>
				</xsl:element>
			</xsl:when>
		<xsl:when test="@render='sub'">
				<xsl:element name="sub">
					<xsl:call-template name="addid"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
		<xsl:when test="@render='super'">
				<xsl:element name="sup">
					<xsl:call-template name="addid"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
		<xsl:when test="@render='smcaps'">
				<xsl:element name="span">
					<xsl:call-template name="addid"/>
					<xsl:attribute name="style">font-variant:small-caps</xsl:attribute>
					<xsl:apply-templates/>
				</xsl:element>
		</xsl:when>
		<xsl:when test="@render='boldsmcaps'">
				<xsl:element name="span">
					<xsl:call-template name="addid"/>
					<xsl:attribute name="style">font-variant:small-caps</xsl:attribute>
					<xsl:element name="b">
						<xsl:apply-templates/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="i">
					<xsl:call-template name="addid"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template match="emph">
	<xsl:call-template name="render"/>
</xsl:template>

<xsl:template match="title">
	<xsl:choose>
		<xsl:when test="@href | @entityref">
			<xsl:element name="a">
				<xsl:attribute name="style">
					<xsl:text>text-decoration:none;color:</xsl:text>
					<xsl:value-of select="$headcolor"/>
				</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:call-template name="thisorthat"/>
				</xsl:attribute>
				<xsl:if test="@show=&quot;new&quot;">
					<xsl:attribute name="target">new</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="render"/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="render"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="bibref | archref">
	<xsl:choose>
		<xsl:when test="@href | @entityref">
			<xsl:element name="a">
				<xsl:attribute name="style">
					<xsl:text>text-decoration:none;color:</xsl:text>
					<xsl:value-of select="$headcolor"/>
				</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:call-template name="thisorthat"/>
				</xsl:attribute>
				<xsl:if test="@show=&quot;new&quot;">
					<xsl:attribute name="target">new</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Template for reference link objects-->
<xsl:template match="ref">
	<xsl:element name="a">
		<xsl:attribute name="style">
			<xsl:text>text-decoration:none;color:</xsl:text>
			<xsl:value-of select="$headcolor"/>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="@target">
				<xsl:attribute name="href">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@target"/>
				</xsl:attribute>
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="@href">
				<xsl:attribute name="href">
					<xsl:value-of select="@href"/>
				</xsl:attribute>
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>
					<xsl:text>Element ref does not have a target or href attribute.!</xsl:text>
					<xsl:text>Link will not function until one or the other attribute is added.</xsl:text>
				</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>

<xsl:template match="ptr">
	<xsl:element name="a">
		<xsl:attribute name="style">
			<xsl:text>text-decoration:none</xsl:text>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="@target">
				<xsl:attribute name="href">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@target"/>
				</xsl:attribute>
				<xsl:element name="span">
					<xsl:attribute name="style">
						<xsl:text>background-color:</xsl:text>
						<xsl:value-of select="$headcolor"/>
						<xsl:text>;vertical-align:text-top</xsl:text>
						<xsl:text>;font-size:xx-small</xsl:text>
						<xsl:text>;color:white</xsl:text>
					</xsl:attribute>
					<xsl:text>N</xsl:text>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@href">
				<xsl:attribute name="href">
					<xsl:value-of select="@href"/>
				</xsl:attribute>
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>
					<xsl:text>Element ref does not have a target or href attribute.!</xsl:text>
					<xsl:text>Link will not function until one or the other attribute is added.</xsl:text>
				</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>

<!-- Digital Archival Objects: Created based upon the possibilities of two digital archival
objects. The First listed is the thumbnail or the sound image. The second is the external
file.  -->

<xsl:template name="daodisplay">
	<xsl:apply-templates select="daogrp/daodesc"/>	
</xsl:template>

<xsl:template name="daolink">
<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:value-of select="daogrp/dao/@href"/>
		</xsl:attribute>
		<xsl:value-of select="daogrp/dao/@title"/>
	</xsl:element>
</xsl:template>

<xsl:template name="daolink1">
<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:value-of select="dao/@href"/>
		</xsl:attribute>
		<xsl:text> </xsl:text><xsl:value-of select="dao/@title"/>
	</xsl:element>
</xsl:template>


<xsl:template name="imagelinks">
<xsl:value-of select="@href"/>
</xsl:template>

<xsl:template name="daodescription">
<xsl:apply-templates select="daodesc"/>
</xsl:template>

<xsl:template match="*//daogrp">
<center>
<table cellspacing="15">
<tr>
<td>
<xsl:element name="a">
	<xsl:attribute name="target">new</xsl:attribute>
	<xsl:attribute name="href">
	<xsl:for-each select="daoloc[@role='reference']">
	<xsl:call-template name="imagelinks"/>
	</xsl:for-each>	
	</xsl:attribute>
	<xsl:element name="img">
	<xsl:attribute name="src">
	<xsl:for-each select="daoloc[@role='thumbnail']">
	<xsl:call-template name="imagelinks"/>
	</xsl:for-each>	
	</xsl:attribute>
	<xsl:attribute name="alt">
	<xsl:value-of select="daoloc[@title]"/>
	<xsl:apply-templates/>
	</xsl:attribute>
	</xsl:element>

</xsl:element>
</td>

<td><b><xsl:call-template name="daodescription"/></b></td></tr></table>
	</center>
</xsl:template>


</xsl:stylesheet>
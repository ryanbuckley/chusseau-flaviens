<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:saxon="http://icl.com/saxon" extension-element-prefixes="saxon">

<!-- Stylesheet created 19 October 2004 by Vikram Ahmed, ECU -->

<!-- To change the color of the Title and Top headings in the document -->
<xsl:param name="headcolor">#3399FF</xsl:param>

<!-- Table of Contents color: To change the color of the Table of Contents -->
<xsl:param name="toccolor">#336600</xsl:param>

<!-- Link color: To change the color of the External Links -->
<xsl:param name="linkcolor">#CC0066</xsl:param>

<!-- Background color: To change the color of the background -->
<xsl:param name="pcolor">#FFFFEE</xsl:param>

<!-- URL: To input the URL of the Repository -->
<xsl:param name="website">http://sils.unc.edu/programs/courses/special_topics.html#210-131</xsl:param>

<!-- Back to top: To change the color of the "back to top" navigations. -->
<xsl:param name="backcolor">#336600</xsl:param>

</xsl:stylesheet>
<!-- Save file as eadcust.xsl in C:\notetab\ead\styles. Replace existing file. -->